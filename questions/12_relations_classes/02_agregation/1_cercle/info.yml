type: prog

titre: Un cercle de couleur

niveau: base

objectif: Utiliser un sous objet dans une relation d'agrégation entre les classes Cercle et Point

énoncé: |
  Soit la classe **Point** qui représente les points d'un plan cartésien, ayant les membres suivants:
  - l'abcisse `x` du point dans le plan
  - l'ordonnée `y` du point dans le plan
  - un **constructeur**  à deux paramètres pour initialiser les attributs `x` et `y`

  Soit la classe **Cercle** qui représente les cercles dans un plan cartésien, définie par les attributs :
  - le  `rayon` du cercle 
  - la `couleur` du cercle
  - le `centre` du cercle qui est un objet de la classe `Point` 
  - un **constructeur**  à deux paramètres pour initialiser les attributs `rayon` et `couleur` 

  Les classes `Cercle` et `Point` sont en relation d'**agrégation**, l'attribut `centre` est nommé **sous-objet**.

  Dans la classe **TestCercle**, on souhaite créer un objet de la classe `Cercle` en utilisant le constructeur personnalisé, à partir des valeurs reçues en entrée pour le `rayon` et la `couleur`, quand au `centre` du cercle, on veut le créer à partir des valeurs reçues en entrée également, pour son abcisse `x` et son ordonnée `y`.  
  On souhaite enfin produire en sortie les valeurs des attributs de l'objet créé ainsi que ceux de son sous-objet, avec des messages explicites selon le modèle des exemples ci-dessous.


  ### Entrées

  - **rayon**: du cercle  (nombre entier)
  - **couleur** : du cercle  (chaine de caractères)
  - **x** : abcisse du centre du cercle (nombre entier)
  - **y** : ordonnée du centre du cercle (nombre entier)

  ### Sortie attendue

    Les valeurs des attributs de l'objet `cercle` créé, selon le modèle des exemples ci-dessous.

  ### À faire

    Compléter le code afin de produire en sortie les valeurs des attributs de l'objet créé de la classe `Cercle` et de son sous-objet `centre` de la classe `Point`

  ### Exemples

    1. Un petit cercle blanc

    - Entrées :
  ```
      2
      blanc
      0
      0

    ```
  
  - Sortie attendue :
  ``` 
    rayon= 2
    couleur= blanc
    x= 0
    y= 0

  ```
  2. Un grand cercle rouge

  - Entrées :
  ```
    10
    rouge
    5
    3

  ```

  - Sortie attendue :
  ```
    rayon= 10
    couleur= rouge
    x= 5
    y= 3

  ```
ébauches:
  java: !include Cercle.java


rétroactions:
  positive: Bravo tu sais manipuler les sous objets dans une relation d'agrégation!
  négative: Les attributs d'un sous-objet doivent être préfixés par l'objet suivi du sous-objet

tests: !include tests.yml

auteur: Wafaa Niar Dinedane / Patrick Lafrance
licence: 2021 CC-BY-SA



