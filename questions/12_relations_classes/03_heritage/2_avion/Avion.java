import java.util.Scanner;
//  Définition de la super classe
class Avion {
	// attributs
	String immatriculation;
    double capaciteReservoir;
	double consommation;
	double carburant;

    // versions constructeur surchargées. À faire
    public Avion(){
    }
    public Avion( String uneImmatriculation ){
		this( +TODO                                               -TODO );
	}
    public Avion( String uneImmatriculation, double uneCapaciteReservoir, double uneConsommation, double unCarburant  ) {
    	// +TODO


		// -TODO
    }

    // méthode qui remplit le carburant à son maximum (capacité réservoir). À faire
	public void faireLePlein(){
		+TODO                                           -TODO
	}
	public void voler( double durée ){
			carburant -= consommation *durée;
	}
}

//  Définition de la sous-classe de la classe Avion. À faire
class A380 +TODO                   -TODO {
	// attributs
	int nombreSieges;
	int passagers;

	// versions constructeur surchargés. À faire
	public A380(){
	}
	public A380( String uneImmatriculation ){
		this( +TODO                                 -TODO );
	}
	public A380( String uneImmatriculation, double uneCapaciteReservoir, double uneConsommation, double unCarburant,  int unNombreSieges, int unPassagers ){
    	  super( +TODO                                       -TODO );         
		  // +TODO


		  // -TODO
	}

	// méthode qui remplit l'avion de passagers à son maximum (nombre de sièges). À faire
	public void embarquer( ){
		+TODO                                       -TODO
	}
	// méthode qui vide l'avion de ses passagers. À faire
	public void débarquer(){
		+TODO                                         -TODO
	}
}

// Définition de la sous classe de la classe Avion. À faire 
class CL215 +TODO                -TODO {
	// attributs
	double capaciteEau;
	double contenuEau;

	// versions constructeur. À faire
	public CL215(){
	}
	public CL215( String uneImmatriculation ){
		+TODO                        -TODO
	}
	public CL215( String uneImmatriculation, double uneCapaciteReservoir, double uneConsommation, double unCarburant, double uneCapaciteEau, double unContenuEau ){
    	  super( +TODO                                      -TODO );         
		  // +TODO


		  // -TODO
	}

	// méthode qui remplit le contenu eau à son maximum (capacité eau). À faire
	public void remplir(){
		+TODO                                   -TODO
	}
	// méthode qui vide l'eau des réservoirs. À faire
	public void larguer(){
		+TODO                                   -TODO
	}
}
// Classe principale
class Main {
	public static void main( String args [] ){
		Scanner sc = new Scanner( System.in );
		// entrée
		double durée = sc.nextDouble();
		
		// Traitement pour avion de type A380
		A380 avion1 = new A380( "299HT08B", 200000, 15000, 10000, 400, 0 );
		avion1.faireLePlein();					
		avion1.embarquer();
		System.out.println( "Avion A380 :" +
						"\nimmatriculation = " + avion1.immatriculation +
						"\ncapacité réservoir = " + avion1.capaciteReservoir +
						"\nconsommation = " + avion1.consommation +	
						"\ncarburant = " + avion1.carburant +		
						"\nnombre sièges = " + avion1.nombreSieges +				
						"\nnombre passagers = " + avion1.passagers
						);		
		avion1.voler( durée );				
		System.out.println( "\ncarburant restant = " + avion1.carburant + " litres" );
		
		//  Traitement pour avion de type CL215. À faire
		// +TODO






// -TODO	
	}
}
