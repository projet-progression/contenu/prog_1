import java.util.Scanner;

// Classe Personne avec encapsulation et constructeur. À faire 
class Personne {
	// +TODO



// -TODO
}

class Employe extends Personne {
	// -VISIBLE
	static int nbreAppels = 0   ;
	// +VISIBLE	
    private String nas; 
    private String numeroEmploye ;
    private double salaire;
    
    public Employe( String nom, String prenom, String nas, String numeroEmploye, double salaire ){
    	super( nom, prenom );
    	this.nas = nas;
    	this.numeroEmploye = numeroEmploye;
    	this.salaire = salaire;
		// -VISIBLE
		nbreAppels++;
		// +VISIBLE
    }

	public String getNas(){
	  	return nas;
	  }
	public String getNumeroEmploye(){
	  	return numeroEmploye;
	  }
	public double getSalaire(){
	  	return salaire;
	  }
	
	public void setNas( String nas ){
	  	this.nas = nas;
	  }
	public void setNumeroEmploye( String numeroEmploye ){
	  	this.numeroEmploye = numeroEmploye;
	  }  
	public void setSalaire( double salaire ){
	  	this.salaire = salaire;
	  }
}
// +TODO
// Classe Secretaire avec encapsulation et constructeur. À faire


// Classe Praticien avec encapsulation et constructeur. À faire
    

// Classe Infirmier avec encapsulation et constructeur. À faire   


// Classe Medecin avec encapsulation et constructeur. À faire   


// Classe Chirurgien avec encapsulation et constructeur. À faire   


// Classe Patient avec encapsulation et constructeur. À faire  


// Classe Medicament avec encapsulation. À faire 



// -TODO
// Classe principale
class  Hopital {
    public static void main(String[] args){
    	Scanner sc = new Scanner( System.in );
		String nom = "Tremblay";
		String prenom = "Jean";	
		String categorie = sc.nextLine();
        
		switch ( categorie ) {
			case "secrétaire" :
					Secretaire secretaire = new Secretaire( nom, prenom, sc.nextLine(), sc.nextLine(), sc.nextDouble(), sc.next(), sc.next() );
					System.out.println ( secretaire.getPrenom() + " " + secretaire.getNom() + " - " + categorie );
					System.out.println( "NAS: " + secretaire.getNas() + " - Numéro Employé: " + secretaire.getNumeroEmploye() );
					System.out.println( "Bureau: " + secretaire.getNumeroBureau() + " - Tel: " + secretaire.getNumeroTelephone() );
					System.out.println( "Salaire: " + secretaire.getSalaire() + " $" );
					break;
					
			// Autres cas. À faire
// +TODO          
			case "infirmier" :
					

				break;
			
			case "médecin" :


				break;
							
			case "chirurgien" :


				break;
				
			case "patient" :


				break;	
// -TODO
		} 
		// -VISIBLE
		if ( Employe.nbreAppels == 0 && !categorie.equals( "patient" ) ){
			System.out.println( "Incorrect, vous n'avez pas utilisé le mécanisme super() dans le constructeur des sous-classes!" );
			}
		// +VISIBLE	
    }
}
