import java.util.Scanner;

class  Point2D {
    int x;
    int y;
      
    public Point2D( int x, int y ){
		this.x = x;
		this.y = y;
	}

	public double distance( Point2D point ) {
		double distance;
	    distance = Math.sqrt( Math.pow( this.x - point.x , 2 ) + Math.pow( this.y - point.y, 2 ) );
		return distance;
      }  
}

// Classe Point3D avec constructeur, hérite de la classe Point2D. À faire
// +TODO



// -TODO
// Classe de démarrage. À faire
class  TestPoint{
    public static void main( String[] args ){
		Scanner sc = new Scanner( System.in ); 

		// Création d'un point de la classe Point2D avec comme attributs les entrées, et sortie des attributs
        Point2D point = new Point2D( sc.nextInt() , sc.nextInt() );
        System.out.println( "point 2D : ( " + point.x + " , " + point.y + " )" );
	
    	// Création d'un point de la classe Point3D avec comme attributs les entrées, et sortie des attributs. À faire
		// +TODO
		


		// Sortie de la distance entre les deux points, par appel de la méthode appropriée. À faire


// -TODO
     }
}
