type: prog

titre: Redéfinir ou surcharger?

niveau: intermédiaire

objectif: Appliquer les principes du polymorphisme dans une hiérarchie de classes de points

énoncé: |
  On définit la classe `Point` qui représente les points dans un plan cartésien à deux dimensions, ayant les membres suivants:
  - l'abcisse `x` du point
  - l'ordonnée `y` du point
  - le **constructeur** à deux paramètre pour initialiser respectivement l'abcisse et l'ordonnée du point créé
  - la méthode **déplacer(a)** qui permet de déplacer le point horizontalement et verticalement de la même distance `a` en paramètre, sur le plan
  - la méthode **déplacer(a, b)** qui permet de déplacer le point horizontalement et verticalement de la distance **(a,b)** en paramètre, sur le plan
  - la méthode **toString()** qui retourne une chaine constituée des valeurs de tous les attributs de la classe dans un format spécifique
  - la méthode **toString(titre)** qui retourne une chaine constituée des valeurs de tous les attributs de la classe précédés du titre en paramètre, dans un format spécifique

  On souhaite définir la classe `Point3D` représentant les points dans l'espace à 3 dimensions, ayant une abcisse `x` et une ordonnée `y` comme les points à deux dimensions mais en plus:
    - l'altitude `z`, une 3ème coordonnée
    - le **constructeur** à 3 paramètres pour initialiser tous les attributs de cette classe.
    - la méthode **déplacer(a)** qui permet de déplacer le point de la même distance `a` en paramètre, au niveau des trois dimensions
    - la méthode **déplacer(a, b)** qui permet de déplacer le point horizontalement et verticalement de la distance **(a,b)** en paramètre, sur le plan
    - la méthode **déplacer(a, b, c)** qui permet de déplacer le point selon les trois dimensions d'un déplacement **(a, b, c)** en paramètre
    - la méthode **toString()** qui retourne une chaine constituée des valeurs de tous les attributs de la classe dans un format spécifique
    - la méthode **toString(titre)** qui retourne une chaine constituée des valeurs de tous les attributs de la classe précédés du titre en paramètre, dans un format spécifique

  La classe `Point3D` hérite les membres de tous les membres de la classe `Point` **sauf le constructeur**.  
    On souhaite **réutiliser** convenablement le constructeur et les méthodes de la super-classe `Point` dans la définition de sa sous-classe `Point3D` à l'aide des mécanismes `super` et `super()`.

  Dans la classe principale `Main`, on souhaite effectuer les traitements suivants: 
    - créer un objet de chacune des classes: `point` de coordonnées **(20,10)** et `point3D` de coordonnées **(20,30,40)**
    - déplacer ces points d'un même `déplacement` reçu en entrée, par appel à la méthode appropriée
    - produire en sortie les attributs de ces objets dans l'ordre de leur création, par appel à la méthode approriée


  ### Entrées

    - `déplacement` : déplacement des coordonnées des deux points créés (nombre entier)


  ### Sortie attendue

    Un message contenant les valeurs des attributs **après déplacement** des deux point (chaine de caractères)

  ### À faire

    Corriger et compléter le code afin de **réutiliser** adéquatement le **constructeur** et les **méthodes** via les mécanismes adéquats, pour produire les messages selon le modèle des exemples ci-dessous

  ### Exemples

    1. Petit déplacement

    - Entrées :
  ```
    10

  ```
  
  - Sortie attendue :

  ``` 
    Le point 2D : Abcisse= 30 - Ordonnée= 20
    Le point 3D : Abcisse= 30 - Ordonnée= 40 - Altitude= 50

  ```
    2. Déplacement nul

    - Entrées :
  ```
    0

  ```
  
  - Sortie attendue :

  ``` 
    Le point 2D : Abcisse= 20 - Ordonnée= 10
    Le point 3D : Abcisse= 20 - Ordonnée= 30 - Altitude= 40

  ```
ébauches:
  java: !include Point.java


rétroactions:
  positive: Bravo tu sais réutiliser les méthodes lorsqu'elles sont redéfinies
  négative: Il faut utiliser l'appel au constructeur de la super classe avec super() et l'appel à la méthode de la superclasse avec super.nomMéthode() mais seulement s'ils ont la même signature. Dans les deux classes, il y a des méthodes surchargées, il faut utiliser dans ce cas le mécanisme this

tests: !include tests.yml

auteur: Wafaa Niar Dinedane / Patrick Lafrance
licence: 2021 CC-BY-SA



