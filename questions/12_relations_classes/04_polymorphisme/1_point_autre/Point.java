import java.util.Scanner;
// super classe
class Point{
	 // -VISIBLE
	 static int nbreAppelsD = 0, nbreAppelsS = 0;
	 // +VISIBLE
	 // attributs
	 int x;
	 int y;
	// constructeur
	 public Point( int x, int y ){
	     // +TODO


		 // -TODO
	 }
	// versions de méthode qui déplace point. À faire
	 public  void  déplacer( int a ) {
		// corriger pour  appeler une autre version avec this. 
	 	// +TODO 
	    this.x += a;
	    this.y += a;
		// -TODO
	 }
	 public  void  déplacer( int a, int b ) {
	 	 // -VISIBLE                      
		 nbreAppelsD ++;
		 // +VISIBLE
	      this.x += a;
	      this.y += b;
	 }
	// versions méthode qui retourne chaine des valeurs d'attributs. À faire
	public  String toString(){
		// corriger pour  appeler une autre version avec this. 
		String message;
		// +TODO
		message = "Abcisse= " + x + " - Ordonnée= " + y;

		// -TODO
		return message;
	 }
	public  String toString( String titre ){
		  // -VISIBLE                      
		  nbreAppelsS ++;
		  // +VISIBLE
	      String message;
		  message = titre + "Abcisse= " + x + " - Ordonnée= " + y;
	      return message;
	 } 
}
// sous classe
class Point3D extends Point{  
	// attribut spécifique
	int z;
	// constructeur 
	public Point3D( int x, int y, int z ){
		 // corriger pour appliquer rédefinition avec super() 
		 // +TODO               
         this.x = x;
	     this.y = y;
		 this.z = z;

		 // -TODO
   	}
	// versions de méthode qui déplace point. À faire
	@Override
	public void  déplacer( int a ){
		// corriger pour appliquer rédefinition avec super.
		// +TODO 
		x += a;
	    y += a; 
		z += a;
		// -TODO
	}
	@Override
	public void  déplacer( int a, int b ){
		// corriger pour appliquer rédefinition avec super.
		// +TODO 
		x += a;
	    y += b; 
		// -TODO		 
	}
	public void  déplacer( int a, int b, int c ){
		x += a;
	    y += b;  
		z += c;
	}
	// versions méthode qui retourne chaine des valeurs d'attributs. À faire
	@Override
	public  String toString(){
		// corriger pour appliquer rédefinition avec super.
		String message;
		// +TODO 
		message = "Abcisse= " + x + " - Ordonnée= " + y + " - Altitude= " + z;

		// -TODO
		return message;
	}
	@Override
    public  String toString( String titre ){
		// corriger pour appliquer rédefinition avec super.
		String message;
		// +TODO 
		message = titre + "Abcisse= " + x + " - Ordonnée= " + y + " - Altitude= " + z; 
		
		// -TODO
		return message;
    }          
}
// classe principale
class Main{
	public static void main( String args[] ) {
		Scanner sc = new Scanner( System.in );
		Point point;
		Point3D point3D;
		int déplacement;
		
		déplacement = sc.nextInt();
				
	   	// création d'un point de coordonnées ( 20, 10 )
	   	+TODO                           -TODO

	   	//	appel de méthode pour déplacer le point et sortie
	    point.déplacer( déplacement );
	    System.out.println( point.toString( "Le point 2D : " ) );
	
		// création d'un point 3D de coordonnées ( 20, 30, 40 );
	   	+TODO                              -TODO
	
	   	//	appel de méthode pour déplacer le point 3D et sortie
	   	+TODO                                 -TODO

	   	+TODO                                 -TODO
	    // -VISIBLE
		if ( Point.nbreAppelsD < 2 | Point.nbreAppelsS < 2)
			System.out.println(  "Incorrect: vous n'avez pas réutilisé une version de méthode de la super-classe dans la sous-classe");
		// +VISIBLE
	 }
}
