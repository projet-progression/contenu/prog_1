import java.util.Scanner;
// Super classe
class Habitation{
    private String adresse;
    private int nbPieces, largeur, longueur, anneeConstruction;
    boolean piscine;
    // Constructeurs. À faire
    public Habitation( String adresse, int nbPieces, int largeur, int longueur, int anneeConstruction, boolean piscine ){
        // +TODO



        // -TODO
    }
    public Habitation( String adresse ){
        +TODO           -TODO ;
    }
    public Habitation(){
        +TODO          -TODO ;
    }
    // Méthodes accesseurs
    public String getAdresse(){
        return adresse;
    }
    public int getNbPieces(){
        return nbPieces;
    }
    public int getLargeur(){
        return largeur;
    }
    public int getLongueur(){
        return longueur;
    }
    public int getAnneeConstruction(){
        return anneeConstruction;
    }
    public boolean getPiscine(){
        return piscine;
    }
    // Méthodes mutateurs
    public void setAdresse( String uneAdresse ){
        adresse = uneAdresse;
    }
    public void setNbPieces( int nombrePieces ){
        nbPieces = nombrePieces;
    }
    public void setLargeur( int uneLargeur ){
        largeur = uneLargeur;
    }
    public void setLongueur( int uneLongueur ){
        longueur = uneLongueur;
    }
    public void setAnneeConstruction( int uneAnneeConstruction ){
        anneeConstruction = uneAnneeConstruction;
    }
    public void setPiscine( boolean unePiscine ){
        piscine = unePiscine;
    }
    // Méthodes. À faire
    public double superficie(){
        return  +TODO         -TODO ;
    }
    public double age( int anneeCourante ){
        return  +TODO              -TODO ;
    }
    public double evaluer( int anneeCourante ){
        final int PRIX_M2 = 500, LIMITE_AGE = 4;  
        final double TAXE = 20;
        double montant;
       // +TODO


       // -TODO
        return montant;
    }
    public String toString(){
        if ( piscine )
            return  ("- Adresse: " + getAdresse() + " - Pièces: " + getNbPieces() +" - largeur: " + getLargeur() + " - longueur: " + getLongueur() + " - Construction: " + getAnneeConstruction() + " - Avec Piscine: " );
        else
            return  ("- Adresse: " + getAdresse() + " - Pièces: " + getNbPieces() +" - largeur: " + getLargeur() + " - longueur: " + getLongueur() + " - Construction: " + getAnneeConstruction() );

    }
 }
// Sous classe. À faire
class HabitationHistorique +TODO         -TODO{
    private String historique;
    private String premierProprietaire;
    // Méthodes accesseurs
    public String getHistorique() {
        return historique;
    }
    public String getPremierProprietaire() {
        return premierProprietaire;
    }
    // Méthodes mutateurs
    public void setHistorique( String historique ) {
        this.historique = historique;
    }
    public void setPremierProprietaire( String premierProprietaire ) {
        this.premierProprietaire = premierProprietaire;
    }
    // Constructeurs. À faire
    public HabitationHistorique( String adresse, int nbrePiece, int largeur, int longueur, int anneeConstruction,  boolean piscine,
                                    String historique, String premierProprietaire ) {
        // +TODO


        // -TODO
    }
    public HabitationHistorique( String adresse, int nbrePiece, int largeur, int longueur, int anneeConstruction,  boolean piscine ) {
        +TODO                               -TODO;
    }
    public HabitationHistorique( String adresse ) {
        +TODO                               -TODO;
    }
    // Méthodes redéfinies. À faire
    @Override   
    public double evaluer( int anneeCourante ){
	    // +TODO


        // -TODO
    }
    @Override
    public String toString(){
        +TODO                                  -TODO ;
    }
}
// Classe principale
class CourTex{
    public static void main(String[] args) {
		Scanner sc = new Scanner( System.in );
		final int NBRE_HABITATION = 100;
		final int ANNEE_COURANTE = sc.nextInt();
        Habitation [] habitations = new Habitation[ NBRE_HABITATION ];
        
        habitations[0] = new Habitation ( "999 chemin duval, H1M2B1, Quebec",  1, 10, 20, 2015, false );
        habitations[1] = new Habitation ( "99 bord de l'eau, S2T4W1, Montreal",  4, 20, 30, 1960, true );
        habitations[2] = new Habitation ( "450 chemin du golf, D1F4R3, Montreal", 0, 0, 0,0, false );
        habitations[3] = new Habitation ( "444 chemin du golf, D1F4R3, Montreal", 3 , 15, 6, 2000, false );
        habitations[4] = new Habitation ( "999 chemin oka, H1M2B1, Montreal", 1, 10, 20, 2015,  false );
        habitations[5] = new HabitationHistorique ( "Pôle Nord H0H 0H0 Canada", 4, 50, 50, 1700, true, "Ancestrale", "Lana" );
        habitations[6] = new HabitationHistorique ( "5150 rue des ormes, H5T 9W1, Montreal", 2, 15, 20, 1850, false, "Patrimoine culturel", "Tremblay" );
        
        // Sorties. À faire
        System.out.println( "Habitations de 2 pièces et plus:" );
        // +TODO



        // -TODO
        System.out.println( "\nHabitations avec piscine:" );
        // +TODO



        // -TODO
        System.out.println( "\nHabitations de plus de 20 ans:" );
        // +TODO



        // -TODO
    }
}
