import java.util.Scanner;
class Point{
	 // -VISIBLE
	 static int nbreAppelsC = 0, nbreAppelsD = 0, nbreAppelsS = 0;
	 // +VISIBLE
	 // attributs
	 int x;
	 int y;
	// constructeur
	 public Point( int x, int y ){
	 	// -VISIBLE                      
		nbreAppelsC ++;
		// +VISIBLE
	      this.x = x;
	      this.y = y;
	 }
	// méthode qui déplace point selon la distance a en paramètre
	 public  void  déplacer( int a ) {
	 	// -VISIBLE                      
		nbreAppelsD ++;
		// +VISIBLE
	      x += a;
	      y += a;
	 }
	// méthode qui retourne la chaine des valeurs de x et y
	public String toString(){
		// -VISIBLE                      
		nbreAppelsS ++;
		// +VISIBLE
	      return "Abcisse= " + x + " - Ordonnée= " + y;
	 }
}
class Point3D extends Point{  
	// attribut spécifique
	int z;
	// constructeur à 3 paramètres. À faire
	public Point3D( int x, int y, int z ){
		// corriger avec super()
		//+TODO 
        this.x = x;
	    this.y = y;      
		this.z = z;
		// -TODO
   	}
   	// méthodes redéfinies. À faire
	@Override
	public void  déplacer( int a ){
		// corriger pour appliquer redéfinition avec super.
		// +TODO 
		x += a;
	    y += a;
		z += a ;

		// -TODO
	}
	@Override
	public  String toString(){
		// Corriger pour appliquer redéfinition avec super.
		// +TODO 	
        return "Abcisse= " + x + " - Ordonnée= " + y + " - Altitude= " + z; 

		// -TODO
    }
}
// classe principale
class Main{
	public static void main( String args[] ) {
		Scanner sc = new Scanner( System.in );
		Point point;
		Point3D point3D;
		int déplacement;
		
		// entrée
		déplacement = sc.nextInt();
		
	   	// création de l'objet point avec les coordonnées ( 20, 10 ). À faire
		+TODO                   -TODO
	
	   	//	appel de méthode pour déplacer le point et sortie
	    point.déplacer( déplacement );
	    System.out.println( point.toString() );
	
		// création de l'objet point3D avec les coordonnées ( 20, 30, 40 ). À faire
		+TODO                            -TODO
	
	   	//	appel de méthode pour déplacer point3D et sortie. À faire
		+TODO                             -TODO
		+TODO                             -TODO
	    // -VISIBLE
		if ( Point.nbreAppelsC < 2 | Point.nbreAppelsD < 2 | Point.nbreAppelsS < 2)
			System.out.println(  "Incorrect: vous n'avez pas réutilisé une version de méthode de la super-classe dans la sous-classe");
		// +VISIBLE
	 }
}
