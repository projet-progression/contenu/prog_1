import java.util.Scanner;
// super-classe abstraite
abstract class Avion {	
	// -VISIBLE
	static int nbreAppels = 0;
	// +VISIBLE
	String immatriculation;
    double capaciteReservoir, consommation, carburant;
    // constructeurs. À faire
    public Avion(){
    }
    public Avion( String uneImmatriculation ){
		+TODO              -TODO;
	}
    public Avion( String uneImmatriculation, double uneCapaciteReservoir, double uneConsommation ) {
    	// +TODO


		// -TODO
    }
    // méthodes
	public void faireLePlein(){
		carburant = capaciteReservoir;
	}
	public void voler( double durée ){
		carburant -= consommation * durée;
	}
	public String toString(){
		// -VISIBLE
		nbreAppels++;
		// +VISIBLE
		return "immatriculation = " + immatriculation +
				"\nconsommation = " + consommation +
				"\ncapacité réservoir = " + capaciteReservoir +
				"\ncarburant = " + carburant;
	}
	// méthodes abstraites. À faire
	public +TODO      -TODO void remplir(); 
	public +TODO      -TODO void vider();  
}
// sous classe de la classe Avion. À faire
class A380 +TODO      -TODO Avion{
	int nombreSieges, passagers;
	// constructeurs
	public A380(){
	}
	public A380( String uneImmatriculation ){
		+TODO                -TODO ;
	}
	public A380( String uneImmatriculation, double uneCapaciteReservoir, double uneConsommation, int unNombreSieges ){
    	+TODO                  -TODO ;
		nombreSieges = unNombreSieges;
	}
	// méthodes redéfinies
	@Override
	public String toString(){
		return +TODO                              -TODO ;
	}
	@Override
	public void remplir(){
		+TODO                 -TODO ;
	}
	@Override
	public void vider(){
		+TODO                 -TODO ;
	}
}
// sous classe de la classe Avion . À faire
class CL215 +TODO      -TODO Avion{
	double capaciteEau, contenuEau;
	// constructeurs
	public CL215(){
	}
	public CL215( String uneImmatriculation ){
		+TODO      -TODO ;
	}
	public CL215( String uneImmatriculation, double uneCapaciteReservoir, double uneConsommation, double uneCapaciteEau ){
		+TODO             -TODO ;
    	capaciteEau = uneCapaciteEau;
	}
	// méthodes redéfinies
	@Override
	public String toString(){
		return +TODO                          -TODO ;
	}
	@Override
	public void remplir(){
		+TODO              -TODO ;
	}
	@Override
	public void vider(){
		+TODO              -TODO; 
	}
}
// classe principale
class Main{
	public static void main( String args [] ){
		Scanner sc = new Scanner( System.in );	
		// entrée de l'option et de la durée du vol
		String option = sc.next();
		double durée = sc.nextDouble();	
		// traitements. À faire
		// +TODO
	

		
// -TODO 
// -VISIBLE
	if ( Avion.nbreAppels == 0 )
		System.out.println( "Atention la méthode toString() de la super-classe n'a pas été utilisée/réutilisée!" );
// +VISIBLE
	}
}
