import java.util.Scanner;

// super-classe interface
interface Mammifère {
	public String  crier() ;
}
// sous classe
 class Canin implements Mammifère{
	@Override
	public  String  crier(){
		return "Wouf Wouf";
	}
 }
 // sous classe. À faire
 class Félin +TODO        -TODO Mammifère{
	@+TODO     -TODO
	public  String  crier(){
		return "Miaou Miaou";
	}
 }
// sous classe. À faire
 class Bovin +TODO         -TODO Mammifère{
	+TODO        -TODO
	public  String  crier(){
		return "Meuh Meuh";
	}
 }
// classe principale
class TestMammifère {	
	public static void main( String [] arg ){
		Scanner sc = new Scanner( System.in );
		final int MAX = 4;
		int option = sc.nextInt();
		// création tableau 
		Mammifère[] mammifères = new Mammifère[ MAX ];
		mammifères[ 0 ] = new Canin();
		mammifères[ 1 ] = new Félin();
		mammifères[ 2 ] = new Bovin();
		
		// otions pour création du dernier élément du tableau. À faire
		switch( option ){
			case ( 1 ): mammifères[ mammifères.length - 1 ] = new Canin();
			            break;
			            
			case ( 2 ): +TODO                     -TODO;
			            break;
			            
			case ( 3 ): +TODO                     -TODO;
		            	break;		               			            
		}
		// sorties. à faire
		for( int i = 0; i < mammifères.length; i++ ){
			+TODO                                 -TODO
		}
	}	    
}



