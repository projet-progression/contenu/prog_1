import java.util.Scanner;

// Super-classe abstraite avec encapsulation
abstract class Mammifère {
	private int age;
	private String nom;
	
	public int getAge(){
		return this.age;
	}
	public void setAge ( int age ){
		this.age = age;
	}
	// méthodes accesseur et mutateur de l'attribut nom. À faire
	// +TODO



	// -TODO

	// méthode abstraite. À faire
	public +TODO       -TODO String  crier();
  }
// Sous classes. À faire 
	class Canin extends Mammifère{
	@Override
	public  String  crier(){
		return "Wouf Wouf";
	}
 }
 class Félin +TODO       -TODO  Mammifère{
	@+TODO        -TODO 
	public  String  crier(){
		return "Miaou Miaou";
	}
 }
 class Bovin +TODO       -TODO  Mammifère{
	@+TODO        -TODO 
	public  String  crier(){
		return "Meuh Meuh";
	}
 }
// classe principale
class TestMammifère {	
	public static void main( String [] arg ){
		Scanner sc = new Scanner( System.in );

		// Création objet de la sous-classe Canin et sorties. À faire 
		Canin coco = new Canin();
		coco.setNom( "Coco" );
		coco.setAge( 10 );
		System.out.println( coco.getNom() + " a " + coco.getAge() + " ans, son cri est : " + coco.crier() );

	    // Création objet de la sous-classe Félin et sorties. À faire 
		Félin fofo = new Félin();
		fofo.setNom( sc.next() );
		fofo.setAge( sc.nextInt() );
		System.out.println( +TODO                            -TODO );
		
		// Création objet de la sous-classe Bovin et sorties. À faire 
		// +TODO



		// -TODO
	    }	    
}



