import java.util.Scanner;
// super classe abstraite
abstract class Employe{	
	String nom;
	String prenom ;
	// constructeur
	public Employe( String nom, String prenom ) {
		this.nom = nom ;
		this.prenom = prenom;
	}
	// méthode
	public String toString(){
		return "Nom: " + nom + " - Prénom: " + prenom;
	}
	// méthode abstraite. À faire
	public +TODO        -TODO double calculerPaye();
}

// sous classe des employés payés à l'heure. À faire
class EmployeHoraire +TODO              -TODO {
  	 double tauxHoraire;
  	 double nbreHeures;
 	 public EmployeHoraire( String nom, String prenom, double taux, double  heures ){
 	 	// +TODO


		// -TODO
 	 }
 	 @Override
 	 public double calculerPaye(){
		return +TODO                       -TODO; 
 	 }
	@Override
	public  String toString(){
		return +TODO                       -TODO; 
	}  
}

// sous classe des employés payés à la commission. À faire
class EmployeCommission +TODO            -TODO {
  	double commission;
 	int quantite;
	public EmployeCommission( String nom, String prenom,  double commission, int quantite ){
 		// +TODO


		// -TODO
 	}
 	@Override
 	public double calculerPaye(){
		final int LIMITE = 100;
		// +TODO


		// -TODO
	}
	@Override
	public  String toString(){
		return +TODO                      -TODO; 
	}
}

// classe principale
class Main{
   public static void main( String[] args ){
		final int MAX = 3;
		Scanner sc = new Scanner( System.in );
		int total = 0;
		int option;

		// création et remplissage du tableau d'employés
		Employe [] employes = new Employe [ MAX ];
	    employes[ 0 ] = new EmployeCommission( "Tremblay", "pierre", 100, 10 );
        employes[ 1 ] = new EmployeHoraire( "Gagnon", "jean", 10, 40 );

		// traitements selon l'option entrée. À faire
		option = sc.nextInt();
		// +TODO



		// -TODO
   }
}

