import java.util.Scanner

fun main() {
    var sc = Scanner(System.`in`)
    // +VISIBLE
    var nombre1: Int
    var nombre2: Int
    var nombre3: Int
    nombre1 = 10
    nombre2 = 20
    nombre3 = 50
    // -VISIBLE
    nombre1 = sc.nextInt()
    nombre2 = sc.nextInt()
    nombre3 = sc.nextInt()

	// +VISIBLE
	// Sortie des variables sur une ligne. À faire
	// +TODO
    println( nombre1 nombre2 nombre3 )

	// -TODO
	// -VISIBLE
}
