import java.util.Scanner;
class exec {
public static void main( String args[] ) {
Scanner sc = new Scanner( System.in );
// +VISIBLE
int nombre1;
int nombre2;
int nombre3;

nombre1 = 10;
nombre2 = 20;
nombre3 = 50;
// -VISIBLE
nombre1 = sc.nextInt();
nombre2 = sc.nextInt();
nombre3 = sc.nextInt();
// +VISIBLE
// Sortie des variables sur une ligne. À faire

System.out.println( nombre1 +TODO         nombre2      -TODO nombre3 );

// -VISIBLE
}
}
