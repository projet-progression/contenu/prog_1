import java.util.Scanner;
class exec {
public static void main( String args[] ) {
// +VISIBLE
String nom = "Bob";
int âge = 18;
// -VISIBLE
Scanner scan = new Scanner( System.in );
nom = scan.nextLine();
âge = scan.nextInt();
// +VISIBLE

System.out.println( +TODO "Bonjour, je m'appelle Bob et j'ai 18 ans!" -TODO );

// -VISIBLE
}
}
