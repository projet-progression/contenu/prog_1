import java.util.Scanner

fun main() {
	// +VISIBLE
    var nom = "Bob"
    var âge = 18
    // -VISIBLE
    var scan = Scanner(System.`in`)
    nom = scan.nextLine()
    âge = scan.nextInt()
    // +VISIBLE
	// +TODO
    println( "Bonjour, je m'appelle" + nom "et j'ai" âge + "ans!" )
	// -TODO
	// -VISIBLE
}
