import java.util.Scanner;

class exec {

public static void main( String args[] ){
// +VISIBLE
String nom = "Bob";
// -VISIBLE
Scanner scan = new Scanner( System.in );
nom = scan.nextLine();
// +VISIBLE

System.out.println( +TODO "Bonjour je m'appelle Bob !" -TODO );

// -VISIBLE
}
}
