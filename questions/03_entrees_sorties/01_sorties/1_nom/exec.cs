using System;

class TestClass
{
	static void Main(string[] args)
	{
// +VISIBLE
String nom = "Bob";
// -VISIBLE

nom = Console.ReadLine();
// +VISIBLE

Console.WriteLine( +TODO "Bonjour, je m'appelle !" -TODO );

// -VISIBLE
	}
}
