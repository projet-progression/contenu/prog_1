import java.util.Scanner

fun main() {
	// +VISIBLE
    var nom = "Bob"
    // -VISIBLE
    var scan = Scanner(System.`in`)
    nom = scan.nextLine()
    // +VISIBLE
	// +TODO
    println("Bonjour, je m'appelle !")


	// -TODO
	// -VISIBLE
}
