fun main() {
	// +VISIBLE
	// Corriger les valeurs des variables. À faire
	// +TODO
    var nom1: String = 'Jean'
    var nom2: String = 'Bob'
    var âge1 = '10'.code
    var âge2 = '20'.code
    var écart = âge2 - âge1

	// -TODO
	// -VISIBLE
    if (écart == 10) {
        println(" ")
    } else {
        println("Les âges ont été mal corrigés !")
    }
    if (!nom1.equals("Jean") || !nom2.equals("Bob")) {
        println("Les noms ont été mal corrigés !")
    }
}
