import java.util.Scanner

fun main() {
    var sc = Scanner(System.`in`)
    // +VISIBLE
    var position: Int
    var nomDuMois = ""

	// Entrée
    position = sc.nextInt()
    when (position) {
        1 -> nomDuMois = "janvier"
        2 -> nomDuMois = "février"
    }
    // Sortie
    println(nomDuMois)
    // -VISIBLE
}
