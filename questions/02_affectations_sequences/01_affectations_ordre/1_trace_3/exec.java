class exec {

public static void main( String args[] ) {
// +VISIBLE
/*
Soient les affectations suivantes :

montant = 100;
prix = 20;
autreMontant = montant
montant = prix;
prix = autreMontant;

Que deviendraient les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/
// -VISIBLE
int montant = 0;
int prix = 0;
int autreMontant = 0;
// +VISIBLE

prix = +TODO     -TODO;

montant = +TODO     -TODO;

// -VISIBLE
if ( prix == 100 && montant == 20 )
    System.out.println( " " );

}
}
