class exec {

public static void main( String args[] ) {
// +VISIBLE
/*
Soient les affectations suivantes :

montant = 100;
prix = 20;
montant = prix;
prix = montant;

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/
// -VISIBLE
int montant = 0;
int prix = 0;
// +VISIBLE

prix = +TODO     -TODO;

montant = +TODO     -TODO;

// -VISIBLE
if ( prix == 20 && montant == 20 )
	System.out.println( " " );

}
}
