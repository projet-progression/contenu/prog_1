"""
Soient les affectations suivantes :

montant = 100
prix = 20
prix = montant

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
"""
# -VISIBLE
montant = 0
prix = 0
# +VISIBLE

prix = +TODO     -TODO

montant = +TODO     -TODO

# -VISIBLE
if prix == 100 and montant == 100:
    print( ' ' )
