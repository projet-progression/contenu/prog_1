class exec {
public static void main( String args[] ) {
// +VISIBLE
int compteur = 0;
int nombre = 0;
int total = 0;

/*
Soit la séquence des affectations suivante :

nombre = 20;
total = 100;
compteur ++;
nombre *= compteur + 4;
total += nombre + 20;

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/

compteur = +TODO     -TODO;

nombre = +TODO     -TODO;

total = +TODO     -TODO;

// -VISIBLE
if ( compteur == 1 && nombre == 100 && total == 220 )
   System.out.println( " " );
}
}
