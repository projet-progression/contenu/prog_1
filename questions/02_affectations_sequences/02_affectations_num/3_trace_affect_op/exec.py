"""
Soit la séquence des affectations suivante :

compteur = 0
nombre = 20
total = 100
compteur += 1
nombre *= compteur + 4
total += nombre + 20

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
"""
# -VISIBLE
compteur = 0
nombre = 0
total = 0
# +VISIBLE

compteur = +TODO     -TODO

nombre = +TODO     -TODO

total = +TODO     -TODO

# -VISIBLE
if compteur == 1 and nombre == 100 and total == 220:
    print( ' ' )
