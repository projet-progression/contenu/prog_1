class exec {
public static void main( String args[] ) {
// +VISIBLE
double nombre1 = 0;
double nombre2 = 0;
double nombre3 = 0;

/*
Soit la séquence des affectations suivante :

nombre1 = 10;
nombre2 = nombre1 + 5 * nombre1 / 2;
nombre3 = nombre2 - nombre1;

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/

nombre1 = +TODO     -TODO;

nombre2 = +TODO     -TODO;

nombre3 = +TODO     -TODO;

// -VISIBLE
if ( nombre1 == 10 && nombre2 == 35 && nombre3 == 25 )
   System.out.println( " " );
}
}
