"""
Soit la séquence des affectations suivante :

nombre1 = 10
nombre2 = nombre1 + 5 * nombre1 / 2
nombre3 = nombre2 - nombre1

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
"""
# -VISIBLE
nombre1 = 0
nombre2 = 0
nombre3 = 0
# +VISIBLE

nombre1 = +TODO     -TODO

nombre2 = +TODO     -TODO

nombre3 = +TODO     -TODO

# -VISIBLE
if nombre1 == 10 and nombre2 == 35 and nombre3 == 25:
    print( ' ' )
