"""
Soit la séquence des affectations suivante :

compteur = 0
min = 0
max = 0
compteur = compteur + 1
min = compteur + 10
max = min + 50

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
"""
# -VISIBLE
min = 0
max = 0
compteur = 0
# +VISIBLE

compteur = +TODO     -TODO

min = +TODO     -TODO

max = +TODO     -TODO

# -VISIBLE
if compteur == 1 and min == 11 and max == 61:
    print( ' ' )
