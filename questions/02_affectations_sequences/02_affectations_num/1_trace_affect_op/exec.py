"""
Soit la séquence des affectations suivante :

nombre = 10
total = 0
nombre += 1         # Équivaut: nombre = nombre + 1
total += nombre     # Équivaut: total = total + nombre

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
"""
# -VISIBLE
nombre = 0
total = 0
# +VISIBLE

nombre = +TODO     -TODO

total = +TODO     -TODO

# -VISIBLE
if nombre == 11 and total == 11:
    print( ' ' )
