class exec {
public static void main( String args[] ) {
// +VISIBLE
int nombre = 0;
int total = 0;

/*
Soit la séquence des affectations suivante :

nombre = 10;
total = 0;
nombre += 1;
total += nombre;

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/

nombre = +TODO     -TODO;

total = +TODO     -TODO;

// -VISIBLE
if ( nombre == 11 && total == 11 )
   System.out.println( " " );
}
}
