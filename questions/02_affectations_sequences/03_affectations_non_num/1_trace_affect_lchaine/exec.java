class exec {
public static void main( String args[] ) {
// +VISIBLE
String mot1;
String mot2;
String mot3;

/*
Soit la séquence des affectations suivante :

mot1 = "Programmer";
mot2 = mot1 + " c'est coder";
mot3 = mot2 + " ! ";

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/

mot1 = +TODO            -TODO;

mot2 = +TODO            -TODO;

mot3 = +TODO                           -TODO;


// -VISIBLE
if ( mot1.equals( "Programmer" ) && mot2.equals( "Programmer c'est coder" ) && mot3.equals( "Programmer c'est coder ! " ) )
   System.out.println( " " );
}
}
