"""
Soit la séquence des affectations suivante :

mot1 = 'Programmer'
mot2 = mot1 + " c'est coder"    
mot3 = mot2 + ' ! '

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
"""
# -VISIBLE
mot1 = ''
mot2 = ''
mot3 = ''
# +VISIBLE

mot1 = +TODO        -TODO

mot2 = +TODO        -TODO

mot3 = +TODO        -TODO

# -VISIBLE
if mot1 == 'Programmer' and mot2 == "Programmer c'est coder" and mot3 == "Programmer c'est coder ! ":
    print( ' ' )
