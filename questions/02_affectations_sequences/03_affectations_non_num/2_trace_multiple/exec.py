
"""
Soit la séquence des affectations suivante :
lettre1 = ':'
lettre2 = ','
mot1 = " Québec "
mot2 = "Montréal "
statut1 = True
statut2 = False
mot2 = mot2 + lettre2 + mot1 + lettre1
lettre2 = lettre1
statut1 = ( lettre1 == lettre2 ) 
statut2 = ( lettre1 != lettre2 )
statut2 = ( statut2 and not statut1 )
mot1 = mot2 + lettre2

Que deviendraient les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
"""

lettre1 = +TODO      -TODO
lettre2 = +TODO      -TODO
mot1 = +TODO         -TODO
mot2 = +TODO         -TODO
statut1 = +TODO      -TODO
statut2 = +TODO      -TODO

# -VISIBLE
if lettre1 == ':' and lettre2 == ':' and mot1 == "Montréal, Québec ::" and mot2 == "Montréal, Québec :" and statut1  and  not statut2:
    print( ' ' )
