class exec {
public static void main( String args[] ) {
// +VISIBLE
char lettre1;
char lettre2;
String mot1;
String mot2;
boolean statut1;
boolean statut2;

/*
Soit la séquence des affectations suivante :
lettre1 = ':';
lettre2 = ',';
mot1 = " Québec ";
mot2 = "Montréal ";
statut1 = true;
statut2 = false;
mot2 = mot2 + lettre2 + mot1 + lettre1;
lettre2 = lettre1;
statut1 = ( lettre1 != lettre2 );
statut2 = ( lettre1 == lettre2 );
statut2 = ( statut2 & !statut1 );
mot1 = mot2 + lettre2;

Que deviendraient les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/

lettre1 = +TODO      -TODO;
lettre2 = +TODO      -TODO;
mot1 = +TODO         -TODO;
mot2 = +TODO         -TODO;
statut1 = +TODO      -TODO;
statut2 = +TODO      -TODO;

// -VISIBLE
if ( lettre1 == ':' && lettre2 == ':' && mot1.equals( "Montréal, Québec ::" ) && mot2.equals( "Montréal, Québec :" ) && statut1  && !statut2 ) 
   System.out.println( " " );
}
}
