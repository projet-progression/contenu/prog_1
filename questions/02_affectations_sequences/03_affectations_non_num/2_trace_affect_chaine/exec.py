"""
Soit la séquence des affectations suivante :

mot1 = 'Python'
mot2 = ' : '
mot3 = mot1 + mot2
mot3 += "Langage de programmation"

Que deviennent les valeurs des variables si les affectations ci - dessus avaient eu lieu ?
"""
# -VISIBLE
mot1 = ''
mot2 = ''
mot3 = ''
# +VISIBLE

mot1 = +TODO         -TODO

mot2 = +TODO         -TODO

mot3 = +TODO                        -TODO

# -VISIBLE
if mot1 == 'Python' and mot2 == ' : ' and mot3 == 'Python : Langage de programmation':
    print( ' ' )
