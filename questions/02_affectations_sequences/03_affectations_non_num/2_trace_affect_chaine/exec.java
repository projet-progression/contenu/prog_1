class exec {
public static void main( String args[] ) {
// +VISIBLE
String mot1;
String mot2;
String mot3;

/*
Soit la séquence des affectations suivante :

mot1 = "Java";
mot2 = " : ";
mot3 = mot1 + mot2;
mot3 += "Langage de programmation";

Que deviendraient les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/

mot1 = +TODO            -TODO;

mot2 = +TODO            -TODO;

mot3 = +TODO            -TODO;

// -VISIBLE
if ( mot1.equals( "Java" ) && mot2.equals( " : " ) && mot3.equals( "Java : Langage de programmation" ) )
   System.out.println( " " );
}
}
