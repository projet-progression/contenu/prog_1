class exec {
public static void main( String args[] ) {
// +VISIBLE
int nombre;
boolean trouvé1;
boolean trouvé2;
boolean trouvé3;
boolean trouvé4;
/*
Soit la séquence des affectations suivante :

nombre = 10;
trouvé1 =( nombre == 10 );
trouvé2 = ! trouvé1;
trouvé3 = trouvé1 || trouvé2;
trouvé4 = trouvé1 && trouvé2;

Que deviennent les valeurs des variables si les affectations ci-dessus avaient eu lieu ?
*/

trouvé1 = +TODO     -TODO;

trouvé2 = +TODO     -TODO;

trouvé3 = +TODO     -TODO;

trouvé4 = +TODO     -TODO;

// -VISIBLE
if ( trouvé1 && !trouvé2 && trouvé3 && !trouvé4 ) {
    System.out.println( " " );
}
}
}
