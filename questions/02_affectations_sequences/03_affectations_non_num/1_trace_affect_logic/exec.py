"""
Soit la séquence des affectations suivante :

nombre = 10
trouvé1 = (nombre == 10)
trouvé2 = not trouvé1
trouvé3 = trouvé1 or trouvé2
trouvé4 = trouvé1 and trouvé2

Que deviennent les valeurs des variables si les affectations ci - dessus avaient eu lieu ?
"""

trouvé1 = +TODO     -TODO

trouvé2 = +TODO     -TODO

trouvé3 = +TODO     -TODO

trouvé4 = +TODO     -TODO

# -VISIBLE
if trouvé1 and not trouvé2 and trouvé3 and not trouvé4:
    print( ' ' )
