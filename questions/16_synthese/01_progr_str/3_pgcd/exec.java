import java.util.Scanner;
class exec {
static Scanner sc = new Scanner( System.in );
static int nombreAppels1 = 0;
static int nombreAppels2 = 0;
static int nombreAppels3 = 0;
static int nombreAppels4 = 0;
// +VISIBLE
public static void main( String args[] ) {
    int x;
    int y;
    // Entrées
    x = sc.nextInt();
    y = sc.nextInt();
    // Sortie
    System.out.println( pgcd( x , y ) );
    // -VISIBLE
    if ( x != 0 && y != 0 && nombreAppels1 ==0 )
        System.out.println( "Incorrect: il manque l'appel de fonction pgdc()!" );
    if ( x != 0 && y != 0 && nombreAppels2 == 0 )
        System.out.println( "Incorrect: il manque l'appel de fonction diviseurs()!" );
    if ( x != 0 && y != 0 && nombreAppels3 == 0 )
        System.out.println( "Incorrect: il manque l'appel de fonction communs()!" );
    if ( x != 0 && y != 0 && nombreAppels4 == 0 )
        System.out.println( "Incorrect: il manque l'appel de fonction plusGrand()!" );

    // +VISIBLE
}

// Fonction qui retourne le pgcd des deux nombres entiers reçus en paramètre. À faire
static int pgcd( int nombre1 , int nombre2 ) {
    // -VISIBLE
    nombreAppels1++;
    // +VISIBLE
    // +TODO


// -TODO
}
// Fonction qui retourne la liste des diviseurs du nombre reçu en paramètre. À faire 
static int[] diviseurs( int nombre ) {
    // -VISIBLE
    nombreAppels2++;
    // +VISIBLE
    // +TODO



// -TODO
}
// Fonction qui retourne la liste des éléments communs des 2 listes reçues en paramètre. À faire 
static int[] communs ( int tab1[], int tab2[] ) {
    // -VISIBLE
    nombreAppels3++;
    // +VISIBLE
    // +TODO



// -TODO
}
// Fonction qui retourne le plus grand élément d'une liste d'entiers reçue en paramètre. À faire
static int plusGrand ( int tab[] ) {
    // -VISIBLE
    nombreAppels4++;
    // +VISIBLE
    // +TODO


// -TODO
}
// -VISIBLE
}
