# Fonction qui retourne le pgcd des deux nombres reçus en paramètre. À faire
def pgcd( nombre1, nombre2 ):
    # -VISIBLE
    global nombre_appels1
    nombre_appels1 += 1
    # +VISIBLE
    # +TODO


    # -TODO
# Fonction qui retourne la liste des diviseurs du nombre reçu en paramètre. À faire
def diviseurs( nombre ):
    # -VISIBLE
    global nombre_appels2
    nombre_appels2 += 1
    # +VISIBLE
    # +TODO


    # -TODO
# Fonction qui retourne la liste des éléments communs des 2 listes reçues en paramètre. À faire 
def communs( tab1, tab2 ):
    # -VISIBLE
    global nombre_appels3
    nombre_appels3 += 1
    # +VISIBLE
    # +TODO


    # -TODO
# Fonction qui retourne le plus grand élément d'une liste d'entiers reçue en paramètre. À faire
def plusGrand( tab ):
    # -VISIBLE
    global nombre_appels4
    nombre_appels4 += 1
    # +VISIBLE
    # +TODO


# -TODO
# Programme principal
# Entrées
x = int( input() )
y = int( input() )
# -VISIBLE
nombre_appels1 = 0
nombre_appels2 = 0
nombre_appels3 = 0
nombre_appels4 = 0
# +VISIBLE
# Sortie
print( pgcd( x, y ) )
# -VISIBLE
if ( x != 0 and y != 0 and nombre_appels1 <= 1 ):
    print( "Incorrect: il manque l'appel de fonction pgcd()!" )
if ( x != 0 and y != 0 and nombre_appels2 <= 1 ):
    print( "Incorrect: il manque l'appel de fonction diviseurs()!" )
if ( x != 0 and y != 0 and nombre_appels3 <= 1 ):
    print( "Incorrect: il manque l'appel de fonction communs()!" )
if ( x != 0 and y != 0 and nombre_appels4 <= 1 ):
    print( "Incorrect: il manque l'appel de fonction plusGrand()!" )
