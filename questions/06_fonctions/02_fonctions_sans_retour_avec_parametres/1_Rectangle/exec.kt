import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
fun main() {
	// -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
	// Variables locales
    var largeur: Int
    var longueur: Int

	// Entrées
    largeur = sc.nextInt()
    longueur = sc.nextInt()
    // Appel des fonctions, les côtés du rectangle sont transmis en paramètre. À faire
    périmètre(largeur, longueur)
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui calcule et produit en sortie le périmètre du rectangle dont les côtés sont reçus en paramètre. À faire
fun périmètre(uneLargeur: Int, uneLongueur: Int) {
    var résultat: Int // Variable locale
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
} // fin de la fonction périmètre
// Fonction qui calcule et produit en sortie l'aire du rectangle dont les côtés sont recus en paramètre. À faire
// +TODO
// -TODO
// -VISIBLE
