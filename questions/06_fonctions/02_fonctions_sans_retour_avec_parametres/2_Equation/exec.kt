import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var compteurAppels = 0

// +VISIBLE
var a = 0.0
var b = 0.0
var c = 0.0
fun main() {
    // Entrées
    a = sc.nextDouble()
    b = sc.nextDouble()
    c = sc.nextDouble()
    // Sortie de l'abcisse et appel de fonction. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// fonction qui calcule la valeur de y=f(x) pour la valeur x reçue en paramètre et la produit en sortie. À faire
fun f(x: Double) {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    //+TODO


	//-TODO
} // fin de la fonction f
// -VISIBLE
