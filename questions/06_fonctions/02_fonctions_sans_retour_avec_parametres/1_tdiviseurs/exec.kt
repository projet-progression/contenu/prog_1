import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
// Programme principal
fun main() {
    // -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
    var nombre: Int
    // Entrée :
    nombre = sc.nextInt()
    // Appel de fonction, nombre est transmis en paramètre. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui produit en sortie les diviseurs du nombre reçu en paramètre. À faire
fun diviseurs(unNombre: Int) {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // Sorties
    // +TODO


    // -TODO
} // fin de la fonction
// -VISIBLE
