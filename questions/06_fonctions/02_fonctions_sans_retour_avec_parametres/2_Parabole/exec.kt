import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var compteurAppels = 0

// +VISIBLE
fun main() {
    // Variables locales au main
    var a: Double
    var b: Double
    var c: Double

    // Entrées
    a = sc.nextDouble()
    b = sc.nextDouble()
    c = sc.nextDouble()
    // Appel des fonctions, les trois coefficient entrés sont transmis en paramètre. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui calcule et produit en sortie l'abcisse x du sommet
// Reçoit en paramètre les trois coefficients du polynôme. À faire
fun abcisse(unA: Double, unB: Double, unC: Double) {
    var x: Double // Variable locale
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
} // fin de la fonction abcisse
// Fonction qui calcule et produit en sortie l'ordonnée y du sommet
// Reçoit en paramètre les trois coefficients du polynôme. À faire
// +TODO
// -TODO
// -VISIBLE
