import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
// Variables globales
var a = 0.0
var b = 0.0
var c = 0.0

// Programme principal
fun main() {
    // -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
    // Entrées
    a = sc.nextDouble()
    b = sc.nextDouble()
    c = sc.nextDouble()

    // Appel des fonctions. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui calcule et produit en sortie l'abcisse x du sommet. À faire
fun abcisse() {
    var x: Double // Variable locale
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
} // fin de la fonction abcisse
// Fonction qui calcule et produit en sortie l'ordonnée y du sommet
// +TODO
// -TODO
// -VISIBLE
