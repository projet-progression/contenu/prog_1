import java.util.Scanner

var compteurAppels = 0
var sc: Scanner? = Scanner(System.`in`)

// +VISIBLE
// Variable globale
var nombre = 0

// Programme principal
fun main() {
    // Entrée
    nombre = sc.nextInt()
    // Appel de fonction. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui produit en sortie les diviseurs de la variable nombre. À faire
fun diviseurs() {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // Sorties
    // +TODO


    // -TODO
} // fin de la fonction
// -VISIBLE
