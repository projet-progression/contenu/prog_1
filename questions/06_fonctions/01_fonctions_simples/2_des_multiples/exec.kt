import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
// Variables globales
var nombre = 0
var min = 0
var max = 0

// Programme principal
fun main() {
    // -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
    // Entrées
    nombre = sc.nextInt()
    min = sc.nextInt()
    max = sc.nextInt()

    // Appel de fonction. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui calcule et produit en sortie les multiples. À faire
fun multiples() {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
} // -VISIBLE
