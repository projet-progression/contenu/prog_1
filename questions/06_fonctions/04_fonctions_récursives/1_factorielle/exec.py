# Fonction récursive qui retourne la factorielle du nombre reçu en paramètre
def factorielle( un_nombre ):
    # -VISIBLE
    global nombre_appels
    nombre_appels += 1
    # +VISIBLE
    if ( un_nombre == 0 ):
        return 1
    # Alternatives où le nombre reçu en paramètre est plus grand que 0. À faire
    # +TODO


# -TODO
# Programme principal
nombre = int( input() )
# -VISIBLE
nombre_appels = 0
# +VISIBLE
print( factorielle( nombre ) )
# -VISIBLE
if ( nombre_appels <= 1 and nombre > 1 ):
    print( "Incorrect: absence de récursivité!" )
# +VISIBLE