import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var nombreAppels = 0

// +VISIBLE
fun main() {
    var nombre: Int
    nombre = sc.nextInt()
    println(factorielle(nombre))
    // -VISIBLE
    if (nombreAppels <= 1 && nombre > 1) println("Incorrect: absence de récursivité!")
    // +VISIBLE
}

// Fonction récursive qui retourne la factorielle du nombre reçu en paramètre.
fun factorielle(unNombre: Int): Int {
    var résultat: Int
    // -VISIBLE
    nombreAppels++
    // +VISIBLE
    if (unNombre == 0) {
        résultat = 1
    }
    // Alternative où le nombre reçu en paramètre est plus grand que 0. À faire
    // +TODO


    // -TODO
    return résultat
} // -VISIBLE
