import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var nombreAppels = 0

// +VISIBLE
fun main() {
    var n: Int
    n = sc.nextInt()
    // Appel et sortie
    println(somme(n))
    // -VISIBLE
    if (nombreAppels <= 1 && n > 1) println("Incorrect: absence de récursivité!")
    // +VISIBLE
}

// Fonction récursive qui retourne la somme, nombre est reçu en paramètre. À faire
fun somme(unN: Int): Int {
    var résultat: Int
    // -VISIBLE
    nombreAppels++
    // +VISIBLE
    if (unN == 1) {
        résultat = +TODO   -TODO
    } else {
        // Appel récursif
        résultat = unN + somme( +TODO   -TODO )
    }
    return résultat
} // -VISIBLE
