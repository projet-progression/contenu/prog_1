# Fonction récursive qui retourne la somme, nombre est reçu en paramètre. À faire
def somme( un_n ):
    # -VISIBLE
    global nombre_appels
    nombre_appels += 1
    # +VISIBLE
    if ( un_n == 1 ):
        résultat = +TODO         -TODO
    else:
        # Appel résursif.
        résultat = +TODO         -TODO 
    return résultat

# Programme principal
n = int( input() )
# -VISIBLE
nombre_appels = 0
# +VISIBLE
print( somme( n ) )
# -VISIBLE
if ( nombre_appels <= 1 and n > 1 ):
    print( "Incorrect: absence de récursivité!" )

