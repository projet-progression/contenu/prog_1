import java.util.Scanner;
class exec {
static Scanner sc = new Scanner( System.in );
static int nombreAppels = 0;
// +VISIBLE
public static void main( String args[] ) {
    int n;
    n = sc.nextInt();
    // Appel et sortie
    System.out.println( somme( n ) );
    // -VISIBLE
    if ( nombreAppels <= 1 && n > 1 )
        System.out.println( "Incorrect: absence de récursivité!" );
    // +VISIBLE
    }

// Fonction récursive qui retourne la somme, n est reçu en paramètre. À faire
public static int somme( int unN ) {
    int résultat;
    // -VISIBLE
    nombreAppels++;
    // +VISIBLE
    if ( unN == 1 ) {
        résultat = +TODO     -TODO;
    }
    else {
        // Appel récursif
        résultat = +TODO                 -TODO;
    }
    return résultat;
}
// -VISIBLE
}
