# Fonction récursive qui retourne la chaine miroir du texte reçu en paramètre. À faire
def miroir( un_texte ):
    # -VISIBLE
    global compteur_appels
    compteur_appels += 1
    # +VISIBLE
    # +TODO


# -TODO
# Programme principal
texte = input()
# -VISIBLE
compteur_appels = 0
# +VISIBLE
print( miroir( texte ) )
# -VISIBLE
if( len( texte ) > 1 and compteur_appels <= 1 ):
    print( "Incorrect: absence de récursivité!" )




