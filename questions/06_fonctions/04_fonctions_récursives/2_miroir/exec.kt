import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var compteurAppels = 0

// +VISIBLE
fun main() {
    var texte: String
    texte = sc.nextLine()
    println(miroir(texte))
    // -VISIBLE
    if (texte.length() > 1 && compteurAppels <= 1) println("Incorrect: absence de récursivité!")
    // +VISIBLE
}

// Fonction récursive qui retourne la chaine miroir du texte reçu en paramètre. À faire
fun miroir(unTexte: String?): String? {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


	// -TODO
} // -VISIBLE
