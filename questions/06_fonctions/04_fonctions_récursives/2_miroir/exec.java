import java.util.Scanner;
class exec {
static Scanner sc = new Scanner( System.in );
static int compteurAppels = 0;
// +VISIBLE
public static void main( String args[] ) {
    String texte;
    texte = sc.nextLine();
    System.out.println( miroir( texte ) );
    // -VISIBLE
    if ( texte.length() > 1 && compteurAppels <= 1 )
        System.out.println( "Incorrect: absence de récursivité!" );
    // +VISIBLE
}

// Fonction récursive qui retourne la chaine miroir du texte reçu en paramètre. À faire
public static String miroir( String unTexte ) {
    // -VISIBLE
    compteurAppels++;
    // +VISIBLE
    // +TODO



// -TODO
}
// -VISIBLE
}
