import java.util.Scanner;
class exec {
static Scanner sc = new Scanner( System.in );
static int compteurAppels = 0;
// +VISIBLE
public static void main( String args[] ) {
    int nombre;
    nombre = sc.nextInt();
    System.out.println( fibonacci( nombre ) );
    // -VISIBLE
    if ( compteurAppels <= 1 && nombre > 1 )
        System.out.println( "Incorrect: absence de récursivité!" );
    // +VISIBLE
}

// Fonction récursive qui retourne le unNombre-ième terme de la suite fibonacci. À faire
public static int fibonacci( int unNombre ) {
  // -VISIBLE
  compteurAppels++;
  // +VISIBLE
  // +TODO


// -TODO
}
// -VISIBLE
}
