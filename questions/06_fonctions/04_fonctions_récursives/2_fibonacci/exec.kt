import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var compteurAppels = 0

// +VISIBLE
fun main() {
    var nombre: Int
    nombre = sc.nextInt()
    println(fibonacci(nombre))
    // -VISIBLE
    if (compteurAppels <= 1 && nombre > 1) println("Incorrect: absence de récursivité!")
    // +VISIBLE
}

// Fonction récursive qui retourne le unNombre-ième terme de la suite fibonacci. À faire
fun fibonacci(unNombre: Int): Int {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


	// -TODO
} // -VISIBLE
