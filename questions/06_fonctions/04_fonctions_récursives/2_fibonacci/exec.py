# Fonction récursive qui retourne le un_nombre-ième terme de la suite de fibonacci. À faire
def fibonacci( un_nombre ):
    # +TODO
    # -VISIBLE
    global compteur_appels
    compteur_appels += 1
    # +VISIBLE


# -TODO
# Programme principal
nombre = int( input() )
# -VISIBLE
compteur_appels = 0
# +VISIBLE
print( fibonacci( nombre ) )
# -VISIBLE
if compteur_appels <= 1 and nombre > 1:
    print( "Incorrect: absence de récursivité!" )



