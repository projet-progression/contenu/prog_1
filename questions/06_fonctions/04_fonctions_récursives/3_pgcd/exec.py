# Fonction récursive qui retourne le pgcd des deux nombres reçus en paramètre. À faire
def pgcd( un_x, un_y ):
    # -VISIBLE
    global nombre_appels
    nombre_appels += 1
    # +VISIBLE
    # +TODO


# -TODO
# Programme principal
# Entrées
x = int( input() )
y = int( input() )
# -VISIBLE
nombre_appels = 0
# +VISIBLE
# Sortie
print( pgcd( x, y ) )
# -VISIBLE
if ( x != 0 and y != 0 and nombre_appels <= 1 ):
    print( "Incorrect: absence de récursivité!" )




