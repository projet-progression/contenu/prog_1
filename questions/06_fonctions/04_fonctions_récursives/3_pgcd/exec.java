import java.util.Scanner;
class exec {
static Scanner sc = new Scanner( System.in );
static int nombreAppels = 0;
// +VISIBLE
public static void main( String args[] ) {
    int x;
    int y;
    // Entrées
    x = sc.nextInt();
    y = sc.nextInt();
    // Sortie
    System.out.println( pgcd( x , y ) );
    // -VISIBLE
    if ( x != 0 && y != 0 && nombreAppels <= 1 )
        System.out.println( "Incorrect: absence de récursivité!" );
    // +VISIBLE
}

// Fonction récursive qui retourne le pgcd des deux nombres entiers reçus en paramètre. À faire
static int pgcd( int unX , int unY ) {
    // -VISIBLE
    nombreAppels++;
    // +VISIBLE
    // +TODO



// -TODO
}
// -VISIBLE
}
