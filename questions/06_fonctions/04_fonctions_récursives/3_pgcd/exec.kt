import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var nombreAppels = 0

// +VISIBLE
fun main() {
    var x: Int
    var y: Int
    // Entrées
    x = sc.nextInt()
    y = sc.nextInt()
    // Sortie
    println(pgcd(x, y))
    // -VISIBLE
    if (x != 0 && y != 0 && nombreAppels <= 1) println("Incorrect: absence de récursivité!")
    // +VISIBLE
}

// Fonction récursive qui retourne le pgcd des deux nombres entiers reçus en paramètre. À faire
fun pgcd(unX: Int, unY: Int): Int {
    // -VISIBLE
    nombreAppels++
    // +VISIBLE
    // +TODO


	// -TODO
} // -VISIBLE
