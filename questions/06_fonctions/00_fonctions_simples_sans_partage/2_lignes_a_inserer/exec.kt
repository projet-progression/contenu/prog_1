import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
// Programme principal
fun main() {
    // -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
    var n: Int

    // Entrée
    n = sc.nextInt()

    // Détermination des carrés et des cubes, appels et Sorties. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0 && n != 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui produit en sortie la ligne. À faire
fun ligne() {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
} // fin de la fonction ligne
// -VISIBLE
