import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
// Programme principal
fun main() {
	// Appel de fonction. À faire
	// +TODO


	// -TODO
	// -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

/**
 * Produit une signature en sortie.
 */
fun signature() {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
} // fin de la fonction signature
// -TODO
// -VISIBLE
