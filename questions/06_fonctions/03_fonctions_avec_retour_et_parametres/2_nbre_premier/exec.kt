import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
fun main() {
    // -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
    var nombre: Int

    // Entrée :
    nombre = sc.nextInt()
    // Appel de fonction, nombre est transmis en paramètre, et Sortie. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
}

// Fonction qui RETOURNE le nombre de diviseurs du nombre reçu en paramètre. À faire
/**
 * Calcule le nombre de diviseurs d'un nombre
 *
 * @param unNombre le nombre dont on cherche les diviseurs
 *
 * @return le nombre de diviseurs de <unNombre>
 */
fun diviseurs(unNombre: Int): Int {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
}
// -VISIBLE
