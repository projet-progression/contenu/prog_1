import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
fun main() {
	// -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
	// Variables locales
    var largeur: Int
    var longueur: Int

    // Entrées
    largeur = sc.nextInt()
    longueur = sc.nextInt()

    // Appel des fonctions avec sortie des résultats
    // les côtés du rectangle sont transmis en paramètre. À faire
    println(périmètre(largeur, longueur))
    //+TODO


    //-TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

/**
 * Calcule le périmètre d'un rectangle
 *
 * @param uneLargeur La largeur du rectangle
 * @param uneLongueur La longueur du rectangle
 * 
 * @return le périmètre du rectangle.
 */
fun périmètre(uneLargeur: Int, uneLongueur: Int): Int {
    var résultat: Int // Variable locale
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


	// -TODO
}
/**
 * Calcule l'aire d'un rectangle
 *
 * @param uneLargeur La largeur du rectangle
 * @param uneLongueur La longueur du rectangle
 * 
 * @return l'aire du rectangle.
 */
// +TODO

// -TODO
// -VISIBLE
