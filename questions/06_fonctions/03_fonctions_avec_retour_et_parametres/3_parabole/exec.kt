import java.util.Scanner

var compteurAppels = 0
var sc = Scanner(System.`in`)

// +VISIBLE
fun main() {
    // Variables locales
    var a: Double
    var b: Double
    var c: Double

    // Entrées
    a = sc.nextDouble()
    b = sc.nextDouble()
    c = sc.nextDouble()
    // Appel des fonctions avec SORTIE des résultats. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (a!=0.0 && compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
}

/**
 * Calcule l'abscisse x du sommet
 *
 *  @param a, b, c les coefficients de la parabole
 *
 *  @return l'abscisse du sommet de la parabole
 */
fun abcisse( a: Double, b: Double, c: Double ): Double {
    var x: Double
    // -VISIBLE
    c=c+0 //Évite les avertissements
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
}

/**
 * Calcule l'ordonnée y du sommet
 *
 *  @param a, b, c les coefficients de la parabole
 *
 *  @return l'ordonnée du sommet de la parabole
 */
// +TODO


// -TODO
// -VISIBLE
