import java.util.Scanner

var compteurAppels = 0
var sc: Scanner? = Scanner(System.`in`)

// +VISIBLE
var a = 0.0
var b = 0.0
var c = 0.0
fun main() {
    var x: Double
    var y: Double
    // Entrées
    a = sc.nextDouble()
    b = sc.nextDouble()
    c = sc.nextDouble()
    // Appel de fonction et sorties. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui retourne la valeur de y=f(x) pour la valeur x reçue en paramètre. À faire
fun f(x: Double): Double {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


	// -TODO
} // fin de la fonction f
// -VISIBLE
