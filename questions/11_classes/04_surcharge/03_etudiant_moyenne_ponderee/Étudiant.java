import java.util.Scanner;

// Définition de la classe Étudiant
class Étudiant {
	// -VISIBLE
	static int nbreAppelsC = 0;
	static int nbreAppelsM = 0;
	static int nbreAppelsR = 0;
	// +VISIBLE
	static final int LIMITE_RÉUSSITE = 60;
	static final int NBRE_NOTES = 4;
	// Attributs
	private String nom;
	private String prénom;
	private String dateNaissance;
	private double[] notes;
	// Version constructeur pour initialiser tous les attributs. À faire
	public Étudiant( String nom, String prénom, String dateNaissance, double[] notes ){
		// -VISIBLE
		nbreAppelsC++;
		// +VISIBLE
		// +TODO

	
	// -TODO		
	}
	// Version constructeur pour initialiser les trois premiers attributs. À faire
	public Étudiant( String nom, String prénom, String dateNaissance ){
		// +TODO

		
	// -TODO
	}
	// Versions méthode qui retourne la réussite ou non de l'étudiant. À faire
	public boolean réussi( int[] pondérations ) {
		// -VISIBLE
		nbreAppelsR++;
		// +VISIBLE
		// +TODO

		
	// -TODO
	}
	public boolean réussi() {
		// +TODO


	// -TODO
	}
	
	// Versions méthode qui retourne la moyenne de l'étudiant. À faire
	public double moyenne( int[] pondérations ) {
		// -VISIBLE
		nbreAppelsM++;
		// +VISIBLE
		// +TODO


	// -TODO	
	}
	public double moyenne() {
		// +TODO


	// -TODO
	}
	// Méthodes accesseurs et mutateurs. À faire
	// +TODO
	


// -TODO
}

class Main {
  	public static void main( String args[] ) {
	    Scanner sc = new Scanner( System.in );
	    final int NBRE_ÉTUDIANTS;
		NBRE_ÉTUDIANTS = sc.nextInt();  // Entrée du nombre d'étudiants
		// Création du tableau d'étudiants
		Étudiant [] étudiants = new Étudiant[ NBRE_ÉTUDIANTS ];
		double [] notesÉtudiants = new double[ Étudiant.NBRE_NOTES ]; 
		int [] pondérationsNotes = new int[ Étudiant.NBRE_NOTES ]; 	
		String option = "";

		// Création des objets du tableau étudiants et entrée de leurs attributs. À faire
		// +TODO




		// Autres traitements et Sorties. À faire
		



		
		// -TODO
		// -VISIBLE
		// Validations
		if ( NBRE_ÉTUDIANTS != 0 ) {
			if ( Étudiant.nbreAppelsC < NBRE_ÉTUDIANTS ){
				System.out.println ( "Incorrect: version générale du constructeur non réutilisée ");
			}	
			if ( Étudiant.nbreAppelsM < NBRE_ÉTUDIANTS || Étudiant.nbreAppelsR < NBRE_ÉTUDIANTS ){
				System.out.println ( "Incorrect: version générale d'une méthode non réutilisée "); 	
			}
		}		
	// +VISIBLE		
 	}
}


