type: prog

titre: Voiture sur le marché

niveau: intermédiaire

objectif: Produire la définition de la classe Voiture

énoncé: |
  On souhaite définir la classe **Voiture** avec les attributs membres suivants:
  - son **immatriculation** 
  - sa **marque**
  - son **modèle**
  - son **année** de fabrication
  - son **prix** de vente neuve

  Ses méthodes membres sont les suivantes:
  - **âge(annéeCourante)** : retourne l'âge de la voiture selon l'année courante en paramètre, en comptant l'année de fabrication 
  comme étant la première année de la voiture
  - **valeur(annéeCourante)** : retourne la valeur courante de la voiture, un nombre entier, selon l'année courante, avec une dépréciation sur le marché des voitures usagées de **25%** la première année, de **15%** la deuxième année, de **10%** la troisième année ainsi que pour chacune des années subséquentes. Une fois sa valeur annulée, elle reste nulle!, la dépréciation étant la **différence** entre le montant payé lors de l’achat du véhicule neuf et son prix de vente sur le marché.
  - **valeurAvecTaxe(tauxTaxe, annéeCourante)** : retourne la valeur de la voiture après taxe (nombre réel) dont le taux est en paramètre pour l'année courante en paramètre.
   
  Dans la classe **TestVoiture**, on souhaite tester la classe **Voiture** en créant un objet `voiture`, et initialiser ses attributs par des valeurs reçues en entrée, pour produire en sortie les valeurs des attributs suivies par les valeur retournées des méthodes membres.

  ### Entrées
    - année courante (nombre entier)
    - taux : de la taxe en pourcentage (nombre entier)
    - immatriculation : de l'objet voiture (chaine de caractères)
    - marque :  de l'objet voiture (chaine de caractères)
    - modèle : de l'objet voiture (chaine de caractères)
    - année : de fabrication de l'objet voiture (nombre entier)
    - prix : à neuf de l'objet voiture (nombre entier)

  ### Sortie attendue

   Les valeurs des attributs de l'objet `voiture` créé suivies par son **âge**,  sa **valeur courante**, et la **valeur après taxe**, chacune sur une
   ligne.

  ### À faire

  Compléter le code pour entrer les valeurs et produire les sorties demandées telles que dans les exemples ci-dessous, chacune sur une ligne.

  ### Exemples

  1. Une Mazda très usagée

   - Entrées :
  ```
    2022
    15
    G91-EFT
    Mazda
    Mazda2
    2012
    25000

  ```

   - Sortie attendue :
  ```
    G91-EFT
    Mazda
    Mazda2
    2012
    25000 $

    11 an(s)
    0 $ avant taxe
    0.0 $ après taxe

  ```
  2. Une Tiguan récente

   - Entrées :
  ```
    2022
    15
    K21-EGK
    Volkswagen
    Tiguan2R
    2022
    30000

  ```

   - Sortie attendue :
  ```
    K21-EGK
    Volkswagen
    Tiguan2R
    2022
    30000

    1 an(s)
    22500 $ avant taxe
    25875.0 $ après taxe
  ```
ébauches:
    java: !include Voiture.java


rétroactions:
    positive: Bravo !
    négative: Les voitures de 8 ans et plus auront une dépréciation de 100% et donc une valeur nulle! Et l'âge zéro compte pour la première année de la voiture.

tests: !include tests.yml

auteur: Wafaa Niar Dinedane / Patrick Lafrance
licence: 2021 CC-BY-SA



