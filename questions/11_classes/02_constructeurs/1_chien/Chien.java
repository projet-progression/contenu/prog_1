import java.util.Scanner;

// Définition de la classe Chien
class Chien {
    // Déclaration des attributs
    String nom;
    int âge;
    // Définition du constructeur sans paramètre 
    public Chien() {
        // Initialisation du nom du chien à "Fido" et de l'âge du chien à 1 an. À faire
        // +TODO


        // -TODO
    }
}

class TestChien {
    public static void main( String args[] ) {
        Scanner sc = new Scanner( System.in );
        // Création d'un objet nommé chien1 de la classe Chien.
        Chien chien1;
        chien1 = new Chien();
        // Création d'un autre objet nommé chien2 de la classe Chien. À faire
        // +TODO



        // -TODO 
        // Sortie des attributs de l'objet chien1. 
        System.out.println( chien1.nom + ", " + chien1.âge + " an" );

        // Sortie des attributs de l'objet chien2. À faire
        // +TODO


        // -TODO
        // -VISIBLE
        if( chien2 == null ) 
            System.out.println( "Erreur vous n'avez pas créé d'objet chien2!" );
        if ( !chien2.nom.equals( "Fido" ) || chien2.âge != 1 )
            System.out.println( "Erreur vous n'avez pas initialisé l'objet chien2!" ); 
    // +VISIBLE
    }
}


