import java.util.Scanner

fun main() {
    var sc = Scanner(System.`in`)
    // +VISIBLE
    var code: Int
    // Sortie des codes numériques des lettres minuscules
    var lettre = 'a'
    while (lettre <= 'z') {
        code = lettre.code
        println(code)
        lettre++
    }

	// Sortie des codes numériques des lettres majuscules. À faire
	// +TODO


	// -TODO
	// -VISIBLE
}
