import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var compteurAppels = 0

// +VISIBLE
fun main() {
    var texte: String
    var texteEnvers: String?
    var palindrome = false

    // Entrée
    texte = sc.nextLine()

    // Appel de fonction
    texteEnvers = envers(texte)

    // Détermination de la valeur de palindrome. À faire
    // +TODO


    // -TODO
    // Sortie. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
}

// Fonction qui retourne le texte reçu en paramètre à l'envers. À faire
fun envers(unTexte: String?): String? {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


	// -TODO
} // -VISIBLE
