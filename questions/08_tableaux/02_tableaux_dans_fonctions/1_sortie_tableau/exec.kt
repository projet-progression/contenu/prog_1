import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var compteurAppels = 0

// +VISIBLE
fun main() {
    val NBRE_ÉLÉMENTS: Int
    NBRE_ÉLÉMENTS = sc.nextInt()
    var tableau = DoubleArray(NBRE_ÉLÉMENTS)

    // Entrée des éléments du tableau
    for (i in 0 until NBRE_ÉLÉMENTS) {
        tableau[i] = sc.nextDouble()
    }
    // Appel de la fonction qui produit en sortie les éléments du tableau transmis en paramètre. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
}

// Fonction qui produit en sortie les éléments du tableau reçu en entrée. Rectifier s'il y a lieu. À faire
// +TODO
fun sortie() {
    // -VISIBLE
    // -TODO
    compteurAppels++
    // +TODO
    // +VISIBLE


	// -TODO
} // -VISIBLE
