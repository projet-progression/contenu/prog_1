import java.util.Scanner

var sc: Scanner? = Scanner(System.`in`)
var compteurAppels = 0

// +VISIBLE
fun main() {
    val NBRE_ÉLÉMENTS: Int
    NBRE_ÉLÉMENTS = sc.nextInt()
    var tableau = DoubleArray(NBRE_ÉLÉMENTS)
    var valeurRecherchée: Double
    var position: Int

    // Entrées
    for (i in 0 until NBRE_ÉLÉMENTS) {
        tableau[i] = sc.nextDouble()
    }
    valeurRecherchée = sc.nextDouble()

    // Appel de fonction et Sortie. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
}

// Fonction qui retourne l'indice de la valeur recherchée dans le tableau à partir de l'indice de départ. À faire
fun rechercher(unTableau: DoubleArray?, uneValeurRecherchée: Double, unIndiceDépart: Int): Int {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


	// -TODO
} // -VISIBLE
