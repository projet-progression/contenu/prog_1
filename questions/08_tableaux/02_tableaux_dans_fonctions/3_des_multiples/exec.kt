import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
// Programme principal
fun main() {
	// -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
    var nombre: Int
    var min: Int
    var max: Int

	// Entrées
    nombre = sc.nextInt()
    min = sc.nextInt()
    max = sc.nextInt()

	// Appel de fonction (nombre, min et max sont transmis en paramètres) et sorties. À faire
	// +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui RETOURNE tous les multiples d'un nombre entre des limites reçues en paramètre dans un tableau d'entiers. À faire
fun multiples(unNombre: Int, unMin: Int, unMax: Int): IntArray? {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // +TODO


    // -TODO
} // -TODO
// -VISIBLE
