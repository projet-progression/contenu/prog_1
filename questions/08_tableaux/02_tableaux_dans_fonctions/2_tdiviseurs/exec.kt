import java.util.Scanner

var compteurAppels = 0

// +VISIBLE
// Programme principal
fun main() {
    // -VISIBLE
    var sc = Scanner(System.`in`)
    // +VISIBLE
    var nombre: Int
    // Entrée :
    nombre = sc.nextInt()
    // Appel de fonction et sorties. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if (compteurAppels == 0) println("Incorrect: pas d'appel de fonction!")
    // +VISIBLE
} // fin du main

// Fonction qui retourne les diviseurs du nombre reçu en paramètre. À faire
fun diviseurs(unNombre: Int): IntArray? {
    // -VISIBLE
    compteurAppels++
    // +VISIBLE
    // Sorties
    // +TODO


    // -TODO
} // fin de la fonction 
// -VISIBLE
