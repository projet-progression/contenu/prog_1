import java.util.Scanner

fun main() {
    var sc = Scanner(System.`in`)
    // +VISIBLE
    val NBRE_GROUPES: Int
    val NBRE_ÉTUDIANTS: Int
    NBRE_GROUPES = sc.nextInt()
    NBRE_ÉTUDIANTS = sc.nextInt()
    var notes = Array<DoubleArray?>(NBRE_GROUPES) { DoubleArray(NBRE_ÉTUDIANTS) }
    var moyennes = DoubleArray(NBRE_GROUPES)
    var somme = 0.0

	// Entrée des notes par groupe d'étudiants
    for (i in 0 until NBRE_GROUPES) {
        for (j in 0 until NBRE_ÉTUDIANTS) {
            notes[i].get(j) = sc.nextDouble()
        }
    }
    // Calcul des moyennes des notes. À faire
	// +TODO


	// -TODO
	// Sortie des moyennes
    for (i in 0 until NBRE_GROUPES) {
        println(moyennes[i])
    }
    // -VISIBLE
}
