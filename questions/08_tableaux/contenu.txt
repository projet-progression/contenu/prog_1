titre: Les tableaux statiques

01_tableaux simples
02_tableaux en paramètre
03_tableaux bidimensionnels

Les tableaux (ou listes ..)

Éléments de contenus:
- Déclaration tableau/ création
- élément de tableau
- indice d'élément dans tableau
- taille de tableau
- parcours de tableau
- algorithmes de recherche, tri, min/max,..
- paramètre tableau dans les fonctions
- etc.



et..les concepts précédents (déclarations - affectation - séquences - entrées/sorties - sélectives - boucles - fonctions - chaines ) mais l'accent est mis sur les tableaux
