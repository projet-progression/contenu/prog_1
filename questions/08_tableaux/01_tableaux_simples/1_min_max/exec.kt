import java.util.Scanner

fun main() {
    var sc = Scanner(System.`in`)
    // +VISIBLE
    val NBRE_TEMPÉRATURES: Int
    NBRE_TEMPÉRATURES = sc.nextInt()
    var températures = DoubleArray(NBRE_TEMPÉRATURES)
    var min = 0.0
    var max = 0.0

	// Entrées des températures
    for (i in 0 until NBRE_TEMPÉRATURES) {
        températures[i] = sc.nextDouble()
    }

	// Calcul du min des températures. 
    if (NBRE_TEMPÉRATURES != 0) {
        min = températures[0]
        for (i in 0 until NBRE_TEMPÉRATURES) {
            if (températures[i] < min) {
                min = températures[i]
            }
        }
    }

	// Calcul du max des températures. À faire
	// +TODO


	// -TODO
	// Sorties. À faire
	// +TODO


	// -TODO
	// -VISIBLE
}
