import java.util.Scanner;
class exec {
public static void main( String args[] ){
Scanner sc = new Scanner( System.in );
// +VISIBLE
final int NBRE_TEMPÉRATURES;
NBRE_TEMPÉRATURES = sc.nextInt();
double températures [] = new double [ NBRE_TEMPÉRATURES ];
double min = 0;
double max = 0;

// Entrées des températures
for ( int i = 0; i < NBRE_TEMPÉRATURES; i++ ) {
	températures[ i ] = sc.nextDouble();
}

// Calcul du min des températures. 
if ( NBRE_TEMPÉRATURES != 0 ){
    min = températures[ 0 ];
    for ( int i = 0; i < NBRE_TEMPÉRATURES; i++ ) {
        if ( températures[ i ] < min ) {
            min = températures[ i ];
    	}
    }
} 

// Calcul du max des températures. À faire
// +TODO



// -TODO
// Sorties. À faire
// +TODO



// -TODO
// -VISIBLE
}
}
