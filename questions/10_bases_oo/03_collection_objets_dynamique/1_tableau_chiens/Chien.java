import java.util.Scanner;
import java.util.ArrayList ;

// Définition de la classe Chien
class Chien {
	// Attributs
	String nom;
	int âge;

	// Méthode
	public String aboyer(){
		return "Woof Woof";
	}
}

class Main {
  	public static void main( String args[] ) {
    	Scanner sc = new Scanner( System.in );
		double sommeÂges = 0;
		double moyenneÂges;
		Chien chien;
		
		// Création du tableau dynamique de chiens. À faire
   		ArrayList< Chien > chiens = new ArrayList< +TODO     -TODO >();

		// Boucle pour créer les objets du tableau chiens dont les attributs sont entrés. À faire
		do{
			chien = new Chien();
			chien.nom = sc.next();
			if ( !chien.nom.equals( "fin" ) ){
				chien.âge = sc.nextInt();
				chiens.add ( chien );
			}
		}while( !chien.nom.equals( "fin" ) );
		
		// Boucle pour déterminer le total des âges des chiens. À faire
		for ( int i = 0; i < +TODO     -TODO; i++ ){
			sommeÂges += +TODO         -TODO ;
		}
	
		// Boucle pour produire en sortie les attributs des chiens du tableau. À faire
		for ( int i = 0; i < +TODO     -TODO ; i++ ){
			System.out.println( +TODO                   -TODO );
		}

		// Sortie de la moyenne des âges des chiens. À faire
		if ( chiens.size() != 0 ){
			moyenneÂges = +TODO          -TODO;
			System.out.println( "moyenne: "+ moyenneÂges + " ans" );
		}
	    
	 	// -VISIBLE	
		for ( int i = 0; i < chiens.size(); i++ ){
			if ( chiens.get( i ) == null ){
				System.out.println( "Erreur: objets du tableau non créés" );
				break;
				}
			if ( chiens.get( i ).nom == null ){
				System.out.println( "Erreur: attributs des chiens non initialisés" );
				break;
				}
		}
	// +VISIBLE
  }
}
