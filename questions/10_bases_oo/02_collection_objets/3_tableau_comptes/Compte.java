
import java.util.Scanner;
// Classe qui définit un compte bancaire
class Compte {
	// attributs
	String codeUsager;
	String motDePasse;
	double solde;

	// méthodes
	public void deposer( double montant ) {
		solde += montant;
	}
	public boolean retirer( double montant ) {
		if ( montant > solde )
			return false;
		solde -= montant;
		return true;
	}
	public void changerMotDePasse( String unMotDePasse ) {
		motDePasse = unMotDePasse;
	}
	public void reinitialiserMotDePasse() {
		motDePasse = "1234";
	}
}
// classe principale.
class GestionCompte {
	final static int MAX = 5;
	public static void main( String args[] ) {
		Scanner sc = new Scanner( System.in );
		Compte comptes[] = new Compte[ MAX ]; 
		int option, i, nbreCompte = 0; 
		String leCodeUsager, leMotDePasse, message = "";

		// Menu répétitif des 4 choix d'option. À faire
		do { 
			option = sc.nextInt();
			switch ( option ) {

				case 1: System.out.println ( "Option 1 : création compte" );
				// +TODO



				// -TODO
				case 2: System.out.println( "Option 2 : sortie des comptes" );
				// +TODO



				// -TODO
				case 3: System.out.println( "Option 3 : sortie d'un compte" );
				// +TODO



				// -TODO
				case 4: System.out.println( "Au revoir!" );
				// +TODO



				// -TODO
				default:
				// +TODO



				// -TODO

			while( +TODO                 -TODO );
				
// -TODO
	}
}
