import java.util.Scanner;

// Définition de la classe Client
class Client {
    // Déclaration des attributs membres
    String nom;
    String prénom;
    String tel;
    String courriel;
    String ville;
}

class TestClient {
  public static void main( String args[] ) {
    Scanner sc= new Scanner( System.in );
    String motPasse;

    // Création objet client1 de la classe Client
     Client client1;
     client1 = new Client();

    // Initialisation des attributs de l'objet client1 dans l'ordre de leur définition
    client1.nom = "Bolduc";
    client1.prénom = "jean";
    client1.tel = "990-345-6789";
    client1.courriel = "jean21@crasemotte.ca";
    client1.ville = "Toronto";

    // Création objet client2 de la classe Client. À faire
    // +TODO



    // -TODO 
    // Initialisation des attributs de l'objet client2 par des valeurs reçues en entrée, dans l'ordre de leur définition. À faire
    client2.nom = sc.next();
    // +TODO



    // -TODO
    // Sortie des attributs de l'objet client2. À faire
    System.out.println( client2.nom + ", " + client2.prénom );
    // +TODO



    // -TODO
    System.out.println(); // saut de ligne
    // Construction et sortie du mot de passe de l'objet client2. À faire
    // +TODO



    // -TODO
    // -VISIBLE
    if( client2 == null ) 
        System.out.println( "Erreur: vous n'avez pas créé d'objet client2!" );
  // +VISIBLE
  }
}


