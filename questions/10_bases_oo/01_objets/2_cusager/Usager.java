import java.util.Scanner;

class Usager {
    // Déclaration des attributs
    String nom;
    String prénom;
    int annéeNaissance;

    // Méthode qui retourne le code usager
    public String codeUsager(){
        return ( prénom.charAt(0) + nom + annéeNaissance ).toLowerCase() ;
    }

    // Méthode qui retourne l'âge de l'usager
    public int ageUsager( int annéeCourante ){
    	return annéeCourante - annéeNaissance;
    }
}

class TestUsager {
  public static void main( String args[] ) {
    Scanner sc= new Scanner( System.in );
    // Entrée de l'année courante
    int annéeCourante = sc.nextInt();
    // Création de l'objet usager1 de la classe Usager. À faire
    // +TODO



    // -TODO 
    // Initialisation de tous les attributs de l'objet usager1 par des valeurs reçues en entrée dans l'ordre de leur définition. À faire
    usager1.nom = sc.next();
    // +TODO



    // -TODO
    // Sortie de tous les attributs de l'objet usager1 dans l'ordre de leur définition. À faire
    // +TODO



    // -TODO
    System.out.println();
    // Sortie du code usager de l'objet usager1 par appel de méthode. À faire
    // +TODO


// -TODO
    // Sortie de l'âge de l'objet usager1 par appel de méthode. À faire
    // +TODO


    // -TODO
    // -VISIBLE
    if( usager1 == null ) 
        System.out.println( "Erreur vous n'avez pas créé d'objet usager1!" );
  // +VISIBLE
  }
}


