import java.util.Scanner;

class  Voiture {
    // Déclaration des attributs
    String immatriculation;
    String marque;
    double carburant;
    double kilométrage;

    // Définition des méthodes
    public void remplir( double quantité ){
        carburant += quantité;
    }
    public void rouler( double distance ){
    	kilométrage += distance;
    }
}

class TestVoiture {
  public static void main( String args[] ) {
    Scanner sc= new Scanner( System.in );
    // Déclaration et création d'un objet
    Voiture voiture1;   
    voiture1 = new Voiture();   
    // Déclaration et création d'un autre objet nommé voiture2. À faire
    // +TODO



    // -TODO
    // Initialisation des attributs de l'objet voiture1
    voiture1.immatriculation = "IFG-199";
    voiture1.marque = "Mazda";
    voiture1.carburant = 0;
    voiture1.kilométrage = 0;

    // Initialisation des attributs de l'objet voiture2 avec les valeurs: IFL-222, Ford, 100 et 100. À faire
    // +TODO



    // -TODO
    // Sortie de tous les attributs de l'objet voiture1 
    System.out.println( voiture1.immatriculation ) ;
    System.out.println( voiture1.marque) ;
    System.out.println( voiture1.carburant ) ;
    System.out.println( voiture1.kilométrage) ;
    System.out.println();

    // Sortie de tous les attributs de l'objet voiture2 dans l'ordre de leur définition dans la classe
    // +TODO



    // -TODO
    System.out.println();
    // Appel de la méthode remplir pour 200 litres de carburant de voiture1
    voiture1.remplir( 200 ); 
    // Appel de la méthode rouler pour une distance de 150 km de voiture1
    // +TODO


    // Sortie des attributs modifiés de l'objet voiture1: carburant et kilométrage


    // -TODO
    // -VISIBLE
    if( ( !voiture2.immatriculation.equals( "IFL-222" ) ) | ( !voiture2.marque.equals( "Ford" ) ) | ( voiture2.carburant != 100 ) | ( voiture2.kilométrage != 100 ) ) 
        System.out.println( "Erreur dans les valeurs assignées aux attributs de voiture2!" );

    if( voiture1.kilométrage != 150  ) 
        System.out.println( "Erreur dans l'appel de la méthode rouler() de voiture1!" );
    // +VISIBLE
    }
}

