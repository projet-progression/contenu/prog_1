#!/bin/bash

if [ -z "$1" ]
then
	rep="."
else
    rep="$1"
fi	

if [ -z "$CI_PAGES_URL" ]
then
  export CI_PAGES_URL="."
fi

if [ -z "$URL_PROGRESSION" ]
then
	if [ "$CI_SERVER_HOST" == "git.dti.crosemont.quebec" ]
	then
		export URL_PROGRESSION="https://progression.dti.crosemont.quebec"
	elif [ "$CI_SERVER_HOST" == "gitlab.com" ]
	then
		export URL_PROGRESSION="https://progression.crosemont.qc.ca"
	fi
fi

vérifier () {
	err=$(python3 -m progression_qc $1 2>&1 >/dev/null)
	if [ $? -eq 1 ]
	then
		echo "* $(get_titre $1) :"
		echo "#+begin_example"
		echo $err
		echo "#+end_example"
		return 1
	else
		return 0
	fi
}

créer_liens () {
	uuid=$(get_champ "uuid" $1)
	if [ -z "$uuid" ]
	then
		uuid=$(dirname $1|md5sum|head -c 32)
	fi

	# Remplacement des variables
	gawk -i inplace "{gsub(/[$]BASE_URL/, \"$CI_PAGES_URL\");}1" $1
	gawk -i inplace "{gsub(/[$]PATH/, \"$uuid\");}1" $1

	mkdir /tmp/q/$uuid
	cp -r $(dirname $1)/* /tmp/q/$uuid

    URL=$CI_PAGES_URL/$uuid/info.yml
	URL_B64=$(echo -n $URL | base64 -w0 | sed "s/=*$//")


    titre=$(get_titre $1)
	
	niveau=$(get_champ "niveau" $1)
	if [ -n "$niveau" ]
	then
		bloc_niveau=" [$niveau]"
	fi

	description=$(get_champ "description" $1)
	if [ -n "$description" ]
	then
		bloc_description="#+begin_quote\n$description\n#+end_quote\n"
	fi
	
	objectif=$(get_champ "objectif" $1)
	if [ -n "$objectif" ]
	then
		bloc_objectif="#+begin_quote\nObjectif : $objectif\n#+end_quote\n"
	fi    

	#bash -c "printf '*%.0s' {1..$(( $2 + 1 ))}"
	echo "@@html:<details><summary>@@"
	echo "[[$URL_PROGRESSION/question?uri=$URL_B64][$titre]]$bloc_niveau"
	printf "$bloc_objectif"
	echo "@@html:</summary>@@"
	printf "$bloc_description"
	echo " {{{voir($URL)}}}"
	echo "@@html:</details>@@"
	echo
}

get_titre () {
	titre=$(get_champ "titre" $1)
	[ -n "$titre" ] && echo $titre || echo $(basename $1)
}

get_champ () {
	champ=$1
	grep -iE "^$champ[ \t]*:" $2|sed "s/^$champ[ \t]*:[ \t]*\(.*\)$/\1/I"
}

chercher () {
	if [ -f $1/contenu.txt ] && [ -n "$(get_titre $1/contenu.txt)" ]
	then
		bash -c "printf '*%.0s' {1..$2}"
		echo " $(get_titre $1/contenu.txt)"
	else	
		find $1 -mindepth 2 -name info.yml -exec bash -c "printf '*%.0s' {1..$2} && echo ' ' $(basename $1)" \; -quit || return
    fi			   

	for f in $(find $1 -mindepth 1 -maxdepth 1 -type d|sort)
	do
		bash -c "chercher $f $(( $2 + 1 ))" \;
	done
		
	find $1 -maxdepth 1 -name info.yml -exec bash -c "vérifier {} && créer_liens {} $2" \;
}

entête () {
	echo "#+setupfile: https://plafrance.pages.dti.crosemont.quebec/org-html-themes/org/theme-readtheorg.setup"
	echo "#+title: $2"
	echo "#+OPTIONS: toc:3"
	echo "#+OPTIONS: num:3"
	echo '#+MACRO: voir @@html:<pre><a class="likeAButton copy" onclick=navigator.clipboard.writeText("src=$1");>📋</a>src=$1</pre></div>@@'
	echo '@@html:<link rel=stylesheet type="text/css" href="./questions.css"/>@@'
}

export -f créer_liens
export -f vérifier
export -f get_titre
export -f get_champ
export -f chercher
export -f entête

dir="$1"

mkdir /tmp/q /tmp/public
cp $dir/public/* /tmp/public

# Production de la liste de questions
entête "$dir/questions" "$(get_titre $dir/questions/contenu.txt)" > /tmp/public/liste_questions.org

for d in $dir/questions
do
    chercher "$d" 1 >> /tmp/public/liste_questions.org
done

# Exportation en HTML
emacs --batch --load $HOME/.emacs.el --load $dir/publish.el --funcall org-publish-all

mv /tmp/q/* /tmp/public/
rm -rf /tmp/q
