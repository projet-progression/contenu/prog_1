créer_liens () {
	uuid=$(get_champ "uuid" $1)
	if [ -z "$uuid" ]
	then
		uuid=$(dirname $1|md5sum|head -c 32)
	fi

	# Remplacement des variables
	gawk -i inplace "{gsub(/[$]BASE_URL/, \"$CI_PAGES_URL\");}1" $1
	gawk -i inplace "{gsub(/[$]PATH/, \"$uuid\");}1" $1

    URL=$CI_PAGES_URL/$uuid/info.yml
	bloc_url="\"url\": \"$URL\","

	titre=$(get_titre $1)
    bloc_titre="\"titre\": \"$titre\","

	niveau=$(get_champ "niveau" $1)
	bloc_niveau="\"niveau\": \"$niveau\","

	description=$(get_champ "description" $1)
	bloc_description="\"description\": \"$description\","

	objectif=$(get_champ "objectif" $1)
	bloc_objectif="\"objectif\": \"$objectif\""

	echo "{"
	echo $bloc_url
	echo $bloc_titre
	echo $bloc_niveau
	echo $bloc_description
	echo $bloc_objectif
	echo "},"
}

get_titre () {
	titre=$(get_champ "titre" $1)
	[ -n "$titre" ] && echo "$titre" || echo "$(basename $(dirname $1))"
}

get_champ () {
	champ=$1
	grep -iE "^$champ[ \t]*:" $2|sed "s/^$champ[ \t]*:[ \t]*\(.*\)$/\1/I"
}

entête () {
	true
}

chercher () {
	echo "{\"nom\": \"$(get_titre $1/contenu.txt)\", "

	echo "\"banques\": ["
	for rep in $(ls -d $1*/)
	do
		find "$rep" -mindepth 2 -type f -name info.yml -print -quit | grep -q info.yml && chercher "$rep" \;
	done

	echo "{}],"

	echo "\"questions\": ["
	for rep in $(ls -d $1*/)
	do
		[ -f "${rep}"info.yml ] && créer_liens "${rep}info.yml"
	done
	[ -f ./info.yml ] && créer_liens ./info.yml

	echo "{}]"

	echo "},"
}

export -f créer_liens
export -f get_titre
export -f get_champ
export -f chercher
export -f entête

dir="$1"

# Production de la liste de questions
entête "$dir/questions" "$(get_titre $dir/questions/contenu.txt)" > /tmp/public/contenu.json

for d in $dir/questions
do
    chercher "$d" 1 >> /tmp/public/contenu.json
done

head -c -2 /tmp/public/contenu.json | python3 -c 'import sys, yaml, json; print(yaml.dump(json.loads(sys.stdin.read()), allow_unicode=True))' | sed '/- {}/d' > /tmp/public/contenu.yml

rm /tmp/public/contenu.json
